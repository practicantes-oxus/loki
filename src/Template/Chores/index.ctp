<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chore[]|\Cake\Collection\CollectionInterface $chores
 */
?>

<!--div class="chores index large-9 medium-8 columns content">
    <h3><?= __('Chores') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('users_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('work_orders_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('chores_status_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($chores as $chore): ?>
            <tr>
                <td><?= $this->Number->format($chore->id) ?></td>
                <td><?= h($chore->name) ?></td>
                <td><?= $chore->has('user') ? $this->Html->link($chore->user->id, ['controller' => 'Users', 'action' => 'view', $chore->user->id]) : '' ?></td>
                <td><?= $chore->has('work_order') ? $this->Html->link($chore->work_order->name, ['controller' => 'WorkOrders', 'action' => 'view', $chore->work_order->id]) : '' ?></td>
                <td><?= $chore->has('chores_status') ? $this->Html->link($chore->chores_status->name, ['controller' => 'ChoresStatus', 'action' => 'view', $chore->chores_status->id]) : '' ?></td>
                <td><?= h($chore->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $chore->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $chore->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $chore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chore->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div-->





    <!-- Content Header (Page header) -->
    <section class="content-header">
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>

    <!-- Main content -->
    
<div class="row">
 <div class="row">
    
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h4>Asignar Tareas</h4>
              <p>Aqui puedes agregar nuevas<br> tareas a tus ordenes de trabajos</p>
            </div>
            <div class="icon">
            <i class="fas fa-tasks"></i>
            </div>
            <a href="#" class="small-box-footer">
            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Asignar tareas  ', ['controller'=>'chores', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

               
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h4>Ordenes de Trabajo</h4>
              <p>Aqui puedes puedes ver <br> todos tus Ordenes de Trabajo</p>
            </div>
            <div class="icon">
            <i class="fas fa-tags"></i>
            </div>
            <a href="#" class="small-box-footer">
            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Ordenes de Trabajo ', ['controller'=>'WorkOrders', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

               
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4>Requerimientos</h4>
              <p>Aqui puedes puedes ver <br> todos tus Requerimientos</p>
            </div>
            <div class="icon">
            <i class="fas fa-address-book"></i>
            </div>
            <a href="#" class="small-box-footer">
            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Ordenes de Trabajo ', ['controller'=>'requeriments', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

               
            </a>
          </div>
        </div>
    </div>
<div class="col-md-3 col-md-push-9">
  <div class="box box-default"><!-- filtro -->
      <div class="box-header with-border">
          <h3 class="box-title">Buscar</h3>
      </div>
          <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                  <?= $this->Form->create() ?>
                                <div class="form-group">
                                    <?if(isset($data_search['users'])){
                                        $instans = $data_search['users'];
                                    }else{
                                     $instans = 0;
                                    }?>
                                    <?= $this->Form->input('users_id', ['label' => 'Usuario', 'options' => $users,'class' => 'form-control','value'=>$instans, 'empty' => 'Seleccione']);?>
                                </div>
                                <div class="form-group">
                                    <?if(isset($data_search['workOrders'])){
                                        $instans = $data_search['workOrders'];
                                    }else{
                                     $instans = 0;
                                    }?>
                                    <?= $this->Form->input('work_orders_id', ['label' => 'Orden de trabajo', 'options' => $workOrders,'class' => 'form-control','value'=>$instans, 'empty' => 'Seleccione']);?>
                                </div>
                                <div class="form-group">
                                    <?if(isset($data_search['choresStatus'])){
                                        $instan = $data_search['choresStatus'];
                                    }else{
                                     $instan = 0;
                                    }?>
                                    <?= $this->Form->control('chores_status_id', ['label' => 'Estado de la tarea', 'options' => $choresStatus,'class' => 'form-control','value'=>$instan, 'empty' => 'Seleccione']);?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                                </div>
                            <?= $this->Form->end() ?>
                  </div>
            </div>
          </div>
        </div>
      </div>
      <!--buscador-->
  <div class="col-md-9 col-md-pull-3">
    <div class="box">
        <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered">
          <tr>
              <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
              <th scope="col"><?= $this->Paginator->sort('Usuario') ?></th>
              <th scope="col"><?= $this->Paginator->sort('Orden de trabajo') ?></th>
              <th scope="col"><?= $this->Paginator->sort('Estado de tarea') ?></th>
              <th scope="col" class="actions"><?= __('Acciones') ?></th>
          </tr>
        <?php foreach ($chores as $chore): ?>
          <tr>
              <td><?= h($chore->name) ?></td>
              <td><?= $chore->has('user') ? $this->Html->link($chore->user->first_name, ['controller' => 'Users', 'action' => 'view', $chore->user->id]) : '' ?></td>
              <td><?= $chore->has('work_order') ? $this->Html->link($chore->work_order->name, ['controller' => 'WorkOrders', 'action' => 'view', $chore->work_order->id]) : '' ?></td>
              <td><?= $chore->has('chores_status') ? $this->Html->link($chore->chores_status->name, ['controller' => 'ChoresStatus', 'action' => 'view', $chore->chores_status->id]) : '' ?></td>
              <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $chore->id], ['class' => 'btn btn-success btn-md', 'escape' => false]) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $chore->id], ['class' => 'btn btn-info btn-md', 'escape' => false]) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $chore->id], ['confirm' => __('Are you sure you want to delete # {0}?', $chore->id), 'class' => 'btn btn-md btn-danger', 'escape' => false]) ?>
              </td>
          </tr>
        <?php endforeach; ?>
        </table>
      </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
              </ul>
          </div>

</div> 


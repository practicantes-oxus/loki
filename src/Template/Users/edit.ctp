<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Users $users
 */
?>
<!--div class="clients form large-9 medium-8 columns content">
    <?= $this->Form->create($user) ?>
    <fieldset>
        <legend><?= __('Edit Users') ?></legend>
        <?php
            echo $this->Form->control('rut');
            echo $this->Form->control('first_name');
            echo $this->Form->control('last_name');
            echo $this->Form->control('phone');
            echo $this->Form->control('email');
            echo $this->Form->control('password');
            echo $this->Form->control('instances_id', ['options' => $instances]);
            echo $this->Form->control('roles_id', ['options' => $roles]);
           
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div-->
                <section class="content-header">
                  
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Usuarios</a></li>
                        <li class="active">Editar</li>
                    </ol>
                </section>
                    <div class="row">
                        <div class="col-md-3 col-md-push-9">

                            <div class="box box-info"><!-- Acciones -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Acciones</h3>
                                </div>
                                <?= $this->Form->create($user) ?>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="btn-toolbar">
                                                <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'users', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                                                <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>     
                                            </div>

                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-md-pull-3">
                        <div class="box box-info">
                            <div class="box-header with-border">
                                <h3 class="box-title">Editar Usuario</h3>
                            </div>
                            
                            <div class="box-body">
                                <div class="row"><!--fila 1 -->
                                    <div class="col-md-4">
                                        <? echo $this->Form->control('rut', ['class' => 'form-control','label' => 'Rut']);?>
                                    </div>
                                    <div class="col-md-4">
                                        <? echo $this->Form->control('first_name', ['class' => 'form-control','label' => 'Nombre']);?>
                                    </div>
                                    <div class="col-md-4">
                                         <? echo $this->Form->control('last_name', ['class' => 'form-control','label' => 'Apellido']);?>
                                    </div>
                                </div>
                                <br>
                                <div class="row"> <!--fila 2 -->
                                    <div class="col-md-4">
                                        <? echo $this->Form->control('phone', ['class' => 'form-control','label' => 'Telefono']);?>
                                    </div>
                                    <div class="col-md-4">
                                        <? echo $this->Form->control('email', ['class' => 'form-control','label' => 'Email']);?>
                                    </div>
                                    <div class="col-md-4">
                                        <? echo $this->Form->control('password', ['class' => 'form-control','label' => 'Contraseña']);?>
                                    </div>
                                </div>    
                                <br>
                                <div class="row"><!-- fila 3 -->
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <? echo $this->Form->control('instances_id', ['label' => 'Empresa', 'options' => $instances,'class' => 'form-control', 'empty' => 'Seleccionar']);?>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-precio">
                                        <? echo $this->Form->control('Role', ['label' => 'Rol', 'options' => $roles,'class' => 'form-control', 'empty' => 'Seleccionar']);?>
                                    </div>
                            </div>
                        </div>
                        <?= $this->Form->end() ?>
                    </div>
                    </div> 
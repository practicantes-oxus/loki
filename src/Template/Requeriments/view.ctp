<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Requeriment $requeriment
 */
?>

<!--div class="requeriments view large-9 medium-8 columns content">
    <h3><?= h($requeriment->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name Requeriments') ?></th>
            <td><?= h($requeriment->name_requeriments) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contact') ?></th>
            <td><?= $requeriment->has('contact') ? $this->Html->link($requeriment->contact->name, ['controller' => 'Contacts', 'action' => 'view', $requeriment->contact->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Requeriments Type') ?></th>
            <td><?= $requeriment->has('requeriments_type') ? $this->Html->link($requeriment->requeriments_type->name, ['controller' => 'RequerimentsTypes', 'action' => 'view', $requeriment->requeriments_type->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prices') ?></th>
            <td><?= h($requeriment->prices) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($requeriment->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($requeriment->date) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Description') ?></h4>
        <?= $this->Text->autoParagraph(h($requeriment->description)); ?>
    </div>
</div-->


<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-body box-profile">
           
            <h3 class="profile-username text-center"><?= h($requeriment->name_requeriment) ?></h3>

            <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= __('Nombre de requerimiento') ?></b> <a class="pull-right"><?= h($requeriment->name_requeriment) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Contactos') ?></b> <a class="pull-right"><?= $requeriment->has('contact') ? $this->Html->link($requeriment->contact->name, ['controller' => 'Contacts', 'action' => 'view', $requeriment->contact->id]) : '' ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Tipos de requerimientos') ?></b> <a class="pull-right"><?= $requeriment->has('requeriments_type') ? $this->Html->link($requeriment->requeriments_type->name, ['controller' => 'RequerimentsTypes', 'action' => 'view', $requeriment->requeriments_type->id]) : '' ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Cotización') ?></b> <a class="pull-right"><?= h($requeriment->prices) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('ID') ?></b> <a class="pull-right"><?= $this->Number->format($requeriment->id) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Fecha') ?></b> <a class="pull-right"><?= h($requeriment->date) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Descripción') ?></b> <a class="pull-right"><?= $this->Text->autoParagraph(h($requeriment->description)); ?></a>
                    </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Tareas</h3>
        </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Fecha') ?></th>
                    </tr>
                </thead>
                <tbody>
                        <? foreach($requeriment->chores as $chore): ?>
                    <tr>
                        <td><?= $this->Number->format($chore->id) ?></td>
                            <td><?= $chore->name ?></td>
                            <td><?= h($chore->date) ?></td>
                    </tr>
                        <? endforeach; ?>
                </tbody>
            </table>

<div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
        <?= $this->Paginator->first(' ' . __('Primera')) ?>
        <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('Siguente') . ' ') ?>
        <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
    </ul>
</div>
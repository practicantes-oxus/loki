<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * WorkOrders Controller
 *
 * @property \App\Model\Table\WorkOrdersTable $WorkOrders
 *
 * @method \App\Model\Entity\WorkOrder[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class WorkOrdersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $requeriments = $this->WorkOrders->Requeriments->find('list', [
            "keyField" => "id",
            "valueField" => "name_requeriment"
        ]);
        $workOrderStatus = $this->WorkOrders->WorkOrderStatus->find('list', ['limit' => 200]);
        //codigo para poder filtrar datos de una instancia segun la cuenta logueada
        $instances = $this->Auth->user()['instances_id'];
        $data_search = $this->request->query;
        $query = $this->WorkOrders->find('search', [
            'search' => $data_search])
        ->contain(['Instances','Requeriments', 'WorkOrderStatus']);

        /*con estas lineas de codigo nuevas se quitar el filtro por instancias al super admin
              pero primero hay que definir la funcion getCurrentdUser en el app controller
            */
            if ($this->getCurrentUser()['roles_id'] !== 4) {
                $query->where(['Requeriments.instances_id'=>$instances]);
            }
        
        $this->paginate = [
            'limit' => 5
        ];
        $workOrders = $this->paginate($query);
        $titleForLayout= 'Ordenes de Trabajo';
        $breadCrumb= 'Inicio';
        

        $this->set(compact('workOrders', 'data_search', 'workOrderStatus', 'breadCrumb', 'requeriments', 'titleForLayout'));
    }

    /**
     * View method
     *
     * @param string|null $id Work Order id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $workOrder = $this->WorkOrders->get($id, [
            'contain' => ['Requeriments', 'WorkOrderStatus', 'chores']
        ]);

        $this->paginate($this->WorkOrders);
        $titleForLayout= 'Ordenes de Trabajo';
        $breadCrumb= 'Vista';
        $this->set(compact('workOrder', $workOrder, 'titleForLayout', 'breadCrumb'));
        $this->WorkOrders->find();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $workOrder = $this->WorkOrders->newEntity();
        if ($this->request->is('post')) {
            $workOrder = $this->WorkOrders->patchEntity($workOrder, $this->request->getData());
            if ($this->WorkOrders->save($workOrder)) {
                $this->Flash->success(__('La orden de trabajo a sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La orden de trabajo no a sido guardado. Intentelo nuevamente.'));
        }
        $requeriments = $this->WorkOrders->Requeriments->find('list', [
            "keyField" => "id",
            "valueField" => "name_requeriment"
        ]);
        $workOrderStatus = $this->WorkOrders->WorkOrderStatus->find('list', ['limit' => 200]);
        $instances = $this->WorkOrders->Instances->find('list', ['limit' => 200]);
        $titleForLayout= 'Ordenes de Trabajo';
        $breadCrumb= 'Agregar';
        $this->set(compact('workOrder', 'requeriments', 'workOrderStatus', 'breadCrumb', 'instances', 'titleForLayout'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Work Order id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $workOrder = $this->WorkOrders->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $workOrder = $this->WorkOrders->patchEntity($workOrder, $this->request->getData());
            if ($this->WorkOrders->save($workOrder)) {
                $this->Flash->success(__('La orden de trabajo a sido editado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La orden de trabajo no a sido editado. Intentelo nuevamente.'));
        }
        $requeriments = $this->WorkOrders->Requeriments->find('list', ['limit' => 200]);
        $workOrderStatus = $this->WorkOrders->WorkOrderStatus->find('list', ['limit' => 200]);
        $titleForLayout= 'Ordenes de Trabajo';
        $breadCrumb= 'Editar';
        $this->set(compact('workOrder', 'requeriments', 'breadCrumb', 'workOrderStatus', 'titleForLayout'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Work Order id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $workOrder = $this->WorkOrders->get($id);
        if ($this->WorkOrders->delete($workOrder)) {
            $this->Flash->success(__('La orden de trabajo a sido eliminada.'));
        } else {
            $this->Flash->error(__('La orden de trabajo no a sido eliminada. Intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

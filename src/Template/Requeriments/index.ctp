<div class="row">
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-blue">
                <div class="inner">
                    <h4>Requerimientos</h4>
                    <p>Aqui puedes puedes<br> agregar nuevos Requerimientos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-address-book"></i>
                </div>
                <a href="#" class="small-box-footer">
                    <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Requerimientos ', ['controller'=>'requeriments', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
                </a>
            </div>
        </div>
        <div class="col-lg-6 col-xs-6">
            <!-- small box -->
            <div class="small-box bg-red">
                <div class="inner">
                    <h4>Agrega Nuevas Ordenes de Trabajo</h4>
                    <p>Aqui puedes puedes agregar <br> nuevas Ordenes de Trabajo</p>
                </div>
                <div class="icon">
                    <i class="fas fa-tags"></i>
                </div>
                <a href="#" class="small-box-footer">
                    <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Ordenes de Trabajo ', ['controller'=>'WorkOrders', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
                </a>
            </div>
        </div>
    </div>

    <!--buscador-->
    <div class="col-md-3 col-md-push-9">
        <div class="box box-default"><!-- filtro -->
            <div class="box-header with-border">
                <h3 class="box-title">Buscar</h3>
            </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->Form->create() ?>
                                <div class="form-group">
                                    <?= $this->Form->input('name_requeriment', ['class' => 'form-control','label' => 'Nombre de requerimiento']);?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->input('contacts_id', ['class' => 'form-control', 'options' => $contacts,'label' => 'Contacto', 'empty' => 'Seleccionar']);?>
                                </div>
                                <div class="form-group">
                                    <?if(isset($data_search['priority_id'])){
                                        $instan = $data_search['priority_id'];
                                    }else{
                                     $instan = 0;
                                    }?>
                                    <?= $this->Form->control('priority_id', ['label' => 'Prioridad', 'options' => $priorityofRequirement,'class' => 'form-control','value'=>$instan,'label' => 'Prioridad', 'empty' => 'Seleccionar']);?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                                </div>
                            <?= $this->Form->end() ?>

                                
                        </div>
                  </div>
                </div>
              </div>
            </div>
            <!--buscador-->


            <!--tabla usuarios-->
<div class="col-md-9 col-md-pull-3">
    <div class="box">
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Nombre del requerimiento') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Contacto') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Tipo de requerimiento') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Cotización') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('Fecha') ?></th>
                    <th scope="col" class="actions"><?= __('Accion') ?></th>
                </tr>
                </thead>
          <tbody>
                <?php foreach ($requeriments as $requeriment): ?>
                <tr>
                      <td><?= $this->Number->format($requeriment->id) ?></td>
                      <td><?= h($requeriment->name_requeriment) ?></td>
                      <td><?= $requeriment->has('contact') ? $this->Html->link($requeriment->contact->name, ['controller' => 'Contacts', 'action' => 'view', $requeriment->contact->id]) : '' ?></td>
                      <td><?= $requeriment->has('requeriments_type') ? $this->Html->link($requeriment->requeriments_type->name, ['controller' => 'RequerimentsTypes', 'action' => 'view', $requeriment->requeriments_type->id]) : '' ?></td>
                      <td><?= h($requeriment->prices) ?></td>
                      <td><?= h($requeriment->date) ?></td>
                      <td class="actions">
                          <?= $this->Html->link(__('Ver'), ['action' => 'view', $requeriment->id], ['class' => 'btn btn-success btn-md']) ?>
                          <?= $this->Html->link(__('Editar'), ['action' => 'edit', $requeriment->id], ['class' => 'btn btn-info btn-md']) ?>
                          <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $requeriment->id], ['class' => 'btn btn-danger btn-md'], ['confirm' => __('Are you sure you want to delete # {0}?', $requeriment->id)]) ?>
                      </td>
                </tr>
                <?php endforeach; ?>
          </tbody>
            </table>
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
              </ul>
          </div>
     </div>
</div>
<!--tabla usuarios-->

 
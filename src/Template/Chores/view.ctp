<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-body box-profile">
           
            <h3 class="profile-username text-center"><?= h($chore->name) ?></h3>

            <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= __('Nombre') ?></b> <a class="pull-right"><?= h($chore->name) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Orden de trabajo') ?></b> <a class="pull-left"><?= $chore->has('work_order') ? $this->Html->link($chore->work_order->name) : '' ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __(' Estado') ?></b> <a class="pull-right"><?= $chore->has('chores_status') ? $this->Html->link($chore->chores_status->name, ['controller' => 'ChoresStatus', 'action' => 'view', $chore->chores_status->id]) : '' ?></a>
                    </li>
                    
                    <li class="list-group-item">
                        <b><?= __('Id') ?></b> <a class="pull-right"><?= $this->Number->format($chore->id) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Fecha') ?></b> <a class="pull-right"><?= h($chore->date) ?></a>
                    </li>
                    <li class="list-group-item">
                        <h4><?= __('Descripción') ?></h4>
                        <?= $this->Text->autoParagraph(h($chore->description)); ?>
                    </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Ordenes de Trabajo</h3>
        </div>
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Role') ?></th>
                    </tr>
                </thead>
                <tbody>
                        <? foreach($workOrders->chores as $chore): ?>
                    <tr>
                        <td><?= $this->Number->format($chore->id) ?></td>
                            <td><?= $chore->name ?></td>
                            <td><?= h($chore->date) ?></td>
                            <td><?= h($chore->description) ?></td>
                    </tr>
                        <? endforeach; ?>
                </tbody>
            </table>

<div class="box-footer clearfix">
        <ul class="pagination pagination-sm no-margin pull-right">
            <?= $this->Paginator->first(' ' . __('Primera')) ?>
            <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguente') . ' ') ?>
            <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
        </ul>
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact $contact
 */
?>
<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Clients'), ['controller' => 'Clients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Client'), ['controller' => 'Clients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contacts form large-9 medium-8 columns content">
    <?= $this->Form->create($contact) ?>
    <fieldset>
        <legend><?= __('Add Contact') ?></legend>
        <?php
            echo $this->Form->control('rut');
            echo $this->Form->control('names');
            echo $this->Form->control('last_name');
            echo $this->Form->control('phone');
            echo $this->Form->control('resource_name');
            echo $this->Form->control('password');
            echo $this->Form->control('clients_id', ['options' => $clients, 'empty' => true]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div> -->





<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Nivel</a></li>
        <li class="active">Aquí</li>
    </ol>
</section>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <div class="box box-info"><!-- Acciones -->
            <div class="box-header with-border">
                <h3 class="box-title">Acciones</h3>
            </div>
            <?= $this->Form->create($contact) ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="btn-toolbar">
                        <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'contacts', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                    <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>
                        </div>                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-9 col-md-pull-3">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Agregar contacto</h3>
            </div>        
            <div class="box-body">
                <div class="row"><!--fila 1 -->
                    <div class="col-md-4">
                        <? echo $this->Form->control('rut', ['class' => 'form-control', 'label' => 'Rut']);?>
                    </div>
                    <div class="col-md-4">
                        <? echo $this->Form->control('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                    </div>
                    <div class="col-md-4">
                         <? echo $this->Form->control('last_name', ['class' => 'form-control', 'label' => 'Apellido']);?>
                    </div>
                </div>
                <br>
                <div class="row"> <!--fila 2 -->
                    <div class="col-md-4">
                        <? echo $this->Form->control('phone', ['class' => 'form-control', 'label' => 'Telefono']);?>
                    </div>
                    <!-- <div class="col-md-4">
                        <? echo $this->Form->control('password', ['class' => 'form-control', 'label' => 'Contraseña']);?>
                    </div> -->
                    <div class="col-md-4">
                        <div class="form-group">
                            <? echo $this->Form->control('client_id', ['label' => 'Empresa', 'options' => $clients,'class' => 'form-control', 'empty' => 'Seleccionar']);?>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <? echo $this->Form->control('instances_id', ['options' => $instances, 'class' => 'form-control', 'label' => 'Instancia']);?>
                    </div>
                </div>    
            </div>
            <?= $this->Form->end() ?>
        </div>
    </div> 
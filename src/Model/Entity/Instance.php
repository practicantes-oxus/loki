<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Instance Entity
 *
 * @property int $id
 * @property string $name
 * @property string $rut_enterprises
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property int $type_enterprises
 * @property int $type_turn
 * @property string $contacts
 * @property \Cake\I18n\FrozenTime $date
 */
class Instance extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'name' => true,
        'rut_enterprises' => true,
        'email' => true,
        'phone' => true,
        'address' => true,
        'type_enterprises' => true,
        'type_turn' => true,
        'contacts' => true,
        'date' => true
    ];
}

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsLog $contactsLog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('List Contacts Log'), ['action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Requeriments'), ['controller' => 'Requeriments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Requeriment'), ['controller' => 'Requeriments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contactsLog form large-9 medium-8 columns content">
    <?= $this->Form->create($contactsLog) ?>
    <fieldset>
        <legend><?= __('Add Contacts Log') ?></legend>
        <?php
            echo $this->Form->control('contacts_id', ['options' => $contacts]);
            echo $this->Form->control('summary');
            echo $this->Form->control('contacts_type');
            echo $this->Form->control('requeriments_id', ['options' => $requeriments]);
            echo $this->Form->control('users_id', ['options' => $users]);
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>

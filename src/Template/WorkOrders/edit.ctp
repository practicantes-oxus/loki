<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\WorkOrder $workOrder
 */
?>
<!--div class="workOrders form large-9 medium-8 columns content">
    <?= $this->Form->create($workOrder) ?>
    <fieldset>
        <legend><?= __('Edit Work Order') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('requeriments_id', ['options' => $requeriments]);
            echo $this->Form->control('work_order_status_id', ['options' => $workOrderStatus]);
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div-->


<section class="content-header">
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Nivel</a></li>
                        <li class="active">Aquí</li>
                    </ol>
                </section>
                    <div class="row">
                        <div class="col-md-3 col-md-push-9">

                            <div class="box box-info"><!-- Acciones -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Acciones</h3>
                                </div>
                                <?= $this->Form->create($workOrder) ?>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="btn-toolbar">
                                                <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'workOrders', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                                                <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>     
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

<div class="col-md-9 col-md-pull-3">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Editar Orden de Trabajo</h3>
        </div>
        <div class="box-body">
            <div class="row"><!--fila 1 -->
                <div class="col-md-4">
                    <? echo $this->Form->control('name', ['class' => 'form-control','label' => 'Nombre']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('requeriments_id', ['options' => $requeriments,'class' => 'form-control', 'empty' => 'Seleccionar','label' => 'Requerimiento']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('work_order_status_id', ['options' => $workOrderStatus, 'class' => 'form-control', 'empty' => 'Seleccionar','label' => 'Estado de la orden de trabajo']);?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <? echo $this->Form->control('date', ['class' => 'form-control','label' => 'Fecha']);?>
                    </div>
                </div>
            </div>
            
        </div>
        <?= $this->Form->end() ?>
    </div>
</div> 
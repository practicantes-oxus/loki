<!--div class="container">

<div class="row" style="margin-top:20px">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
    <?= $this->Flash->render('auth') ?>
		<?= $this->Form->create() ?>
			<fieldset>
				<h2>Ingrese sus datos</h2>
				<hr class="colorgraph">
				<div class="form-group">
                    <?= $this->Form->input('email', ['class' => 'form-control input-lg', 'placeholder' => 'Correo electrónico', 'label' => false, 'require']);?>
				</div>
				<div class="form-group">
					<?= $this->Form->input('password', ['class' => 'form-control input-lg', 'placeholder' => 'Contraseña', 'label' => false, 'require']);?>
				</div>
				<hr class="colorgraph">
				<div class="row">
					<div class="col-xs-6 col-sm-6 col-md-6">
						<?= $this->Form->button('Acceder', ['class' => 'btn btn-lg btn-success btn-block'])?>
					</div>
					
				</div> 
			</fieldset>
            <?= $this->Form->end() ?>
	</div>/**/
</div-->




<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="#"><b>Loki</b>Proyect</a>
		</div>
		<!-- /.login-logo -->
	<div class="login-box-body">
		<p class="login-box-msg">Ingresa tus datos para Iniciar sesion</p>
		<?= $this->Form->create() ?>
	<form action="../../index2.html" method="post">
		<div class="form-group has-feedback">
			<?= $this->Form->input('email', ['class' => 'form-control input-lg', 'placeholder' => 'Correo electrónico', 'label' => false, 'require']);?>
			<span class="glyphicon glyphicon-envelope form-control-feedback"></span>
		</div>
		<div class="form-group has-feedback">
			<?= $this->Form->input('password', ['class' => 'form-control input-lg', 'placeholder' => 'Contraseña', 'label' => false, 'require']);?>
			<span class="glyphicon glyphicon-lock form-control-feedback"></span>
		</div>
		<div class="row">
			<div class="col-xs-12">
				<?= $this->Form->button('Acceder', ['class' => 'btn btn-lg btn-success btn-block'])?>
			</div>
				<?= $this->Form->end() ?>
			<!-- /.col -->
		</div>
	</form>
<!-- /.login-box-body -->
</div>
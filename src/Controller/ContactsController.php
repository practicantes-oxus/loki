<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Contacts Controller
 *
 * @property \App\Model\Table\ContactsTable $Contacts
 *
 * @method \App\Model\Entity\Contact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
    }

    public function index()
    {
         //codigo para poder filtrar datos de una instancia segun la cuenta logueada
         $instances = $this->Auth->user()['instances_id'];
         $data_search = $this->request->query;
         $query = $this->Contacts->find('search', [
             'search' => $data_search])
                 ->where(['Contacts.instances_id'=>$instances])
                 ->contain(['Instances','Clients']);
          
          $this->paginate = [
              'limit' => 5
          ];

        $clients = $this->Contacts->Clients->find('list', ['limit' => 200]);
        $data_search = $this->request->query;
        $query = $this->Contacts->find('search', [
            'search' => $data_search,
            'contain' => [
                'Clients'
            ]
        ]);
        
         $contacts = $this->paginate($query);
         $titleForLayout= 'Contactos';
         $breadCrumb= 'Editar';
 

        $this->set(compact('contacts', 'clients','data_search', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * View method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => ['Clients', 'Requeriments']
        ]);
        $this->paginate($this->Contacts);

        $titleForLayout= 'Contactos';
        $breadCrumb= 'Editar';
        $this->set(compact('contact', $contact, 'titleForLayout', 'breadCrumb'));
        $this->Contacts->find();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contact = $this->Contacts->newEntity();
        if ($this->request->is('post')) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('El contacto se a guardado correctamente'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El contacto no pudo guardarce. Intentelo nuevamente'));
        }
        $clients = $this->Contacts->Clients->find('list', ['limit' => 200]);
        $instances = $this->Contacts->Instances->find('list', ['limit' => 200]);
        $titleForLayout= 'Contactos';
        $breadCrumb= 'Editar';
        $this->set(compact('contact', 'clients', 'instances', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contact = $this->Contacts->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contact = $this->Contacts->patchEntity($contact, $this->request->getData());
            if ($this->Contacts->save($contact)) {
                $this->Flash->success(__('El contacto a sido editado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El contacto no pudo editarce. Intentelo nuevamente.'));
        }
        $clients = $this->Contacts->Clients->find('list', ['limit' => 200]);
        $titleForLayout= 'Contactos';
        $breadCrumb= 'Editar';
        $this->set(compact('contact', 'clients', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contact = $this->Contacts->get($id);
        if ($this->Contacts->delete($contact)) {
            $this->Flash->success(__('El contacto a sido eliminado.'));
        } else {
            $this->Flash->error(__('El contacto no pudo eliminarse. Intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * ContactsLog Model
 *
 * @property \App\Model\Table\ContactsTable|\Cake\ORM\Association\BelongsTo $Contacts
 * @property \App\Model\Table\RequerimentsTable|\Cake\ORM\Association\BelongsTo $Requeriments
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 *
 * @method \App\Model\Entity\ContactsLog get($primaryKey, $options = [])
 * @method \App\Model\Entity\ContactsLog newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\ContactsLog[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\ContactsLog|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactsLog|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\ContactsLog patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\ContactsLog[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\ContactsLog findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactsLogTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('contacts_log');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Contacts', [
            'foreignKey' => 'contacts_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Requeriments', [
            'foreignKey' => 'requeriments_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('summary')
            ->maxLength('summary', 255)
            ->requirePresence('summary', 'create')
            ->notEmpty('summary');

        $validator
            ->integer('contacts_type')
            ->requirePresence('contacts_type', 'create')
            ->notEmpty('contacts_type');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['contacts_id'], 'Contacts'));
        $rules->add($rules->existsIn(['requeriments_id'], 'Requeriments'));
        $rules->add($rules->existsIn(['users_id'], 'Users'));

        return $rules;
    }
}




  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class=" main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      
        <li class="treeview">
          <a href="#" class=" text-white">
          <i class="fas fa-clone"></i></i> <span> Home</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu  text-white">
            <li><?= $this->Html->link(__('Inicio'), ['controller' => 'Users', 'action' => 'home']) ?></li>
          </ul>
        </li>
        
<?
        // || => OR
        // &&
?>
        <? if ($current_user['roles_id'] !== 3) {?>
        <li class="treeview">
          <a href="#" class=" text-white">
          <i class="fas fa-address-book"></i> <span> Clientes y Contactos</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu  text-white">
            <li><?= $this->Html->link(__('Clientes'), ['controller' => 'clients', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Contactos'), ['controller' => 'contacts', 'action' => 'index']) ?></li>
          </ul>
        </li>
        <? } ?>

        <li class="treeview">
          <a href="#" class=" text-white">
          <i class="fas fa-briefcase"> </i>  <span> Desarrollo</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu text-white">
            <li><?= $this->Html->link(__('Requerimientos'), ['controller' => 'requeriments', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(__('Ordenes de Trabajo'), ['controller' => 'WorkOrders', 'action' => 'index']) ?></li>
            <li><?= $this->Html->link(_('Tareas'), ['controller' => 'chores', 'action' => 'index'])?></li>
          </ul>
        </li>


        <li class="treeview">
          <a href="#" class=" text-white">
          <i class="fas fa-users"></i> <span> Gestion Interna</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu  text-white">
            <li><?= $this->Html->link(__('Usuarios'), ['controller' => 'users', 'action' => 'index']) ?></li>


            <li><?
            if ($current_user['roles_id'] == 4) {
                echo $this->Html->link(__('Instancias'), ['controller' => 'instances', 'action' => 'index']); 
             }?>
            </li>
            


          </ul>
        </li>
        
          
    </section>
    <!-- /.sidebar -->
  </aside>

  
<?php
if (!isset($params['escape']) || $params['escape'] !== false) {
    $message = h($message);
}
?>
<div class="alert alert-success alert-dismissible" onclick="this.classList.add('hidden')">
    <button type="button" class="close" data-dismiss="alert"  aria-hidden="true">&times;</button>
        <h4><i class="icon fa fa-check"></i> Alerta</h4>
        <?= $message ?>
</div>
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Contact[]|\Cake\Collection\CollectionInterface $contacts
 */
?>
<!-- <nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Clients'), ['controller' => 'Clients', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Client'), ['controller' => 'Clients', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contacts index large-9 medium-8 columns content">
    <h3><?= __('Contacts') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('rut') ?></th>
                <th scope="col"><?= $this->Paginator->sort('names') ?></th>
                <th scope="col"><?= $this->Paginator->sort('last_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('resource_name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('password') ?></th>
                <th scope="col"><?= $this->Paginator->sort('clients_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?= $this->Number->format($contact->id) ?></td>
                <td><?= h($contact->rut) ?></td>
                <td><?= h($contact->names) ?></td>
                <td><?= h($contact->last_name) ?></td>
                <td><?= h($contact->phone) ?></td>
                <td><?= h($contact->resource_name) ?></td>
                <td><?= h($contact->password) ?></td>
                <td><?= $contact->has('client') ? $this->Html->link($contact->client->name, ['controller' => 'Clients', 'action' => 'view', $contact->client->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contact->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contact->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contact->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div> -->



<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Usuarios</a></li>
        <li class="active">Inicio de usuarios</li>
    </ol>
</section>
<div class="row">
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h4>Registro de Contactos</h4>
                    <p>Aqui puedes agregar un <br> nuevo Contacto</p>
                </div>
                <div class="icon">
                    <i class="fas fa-address-book"></i>
                </div>
                <a href="#" class="">
                    <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Nuevo Contacto ', ['controller'=>'Contacts', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
            <div class="small-box bg-aqua">
                <div class="inner">
                    <h4>Requerimientos</h4>
                    <p>Aqui puedes puedes ver <br> todos tus Requerimientos</p>
                </div>
                <div class="icon">
                    <i class="fas fa-address-book"></i>
                </div>
                <a href="#" class="small-box-footer">
                        <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Requerimientos ', ['controller'=>'requeriments', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>   
                 </a>
                </div>
            </div>
            <div class="col-lg-4 col-xs-4">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h4>Ordenes de Trabajo</h4>
                        <p>Aqui puedes puedes ver <br> todos tus Ordenes de Trabajo</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-tags"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Ordenes de Trabajo ', ['controller'=>'WorkOrders', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>   
                    </a>
                </div>
            </div> 
        </div>

<div class="row">
    <!--buscador-->
    <div class="col-md-3 col-md-push-9">
        <div class="box box-default"><!-- filtro -->
            <div class="box-header with-border">
                <h3 class="box-title">Buscar</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <?= $this->Form->create() ?>
                            <div class="form-group">
                                <?= $this->Form->input('rut', ['class' => 'form-control', 'label' => 'Rut']);?>
                            </div>
                            <div class="form-group">
                                <? echo $this->Form->input('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                            </div>
                            <div class="form-group">
                                <?if(isset($data_search['clients_id'])){
                                    $cliente = $data_search['clients_id'];
                                }else{
                                    $cliente = 0;
                                }?>
                                <? echo $this->Form->input('clients_id', ['label' => 'Cliente', 'empty'=>'seleccione', 'options' => $clients,'class' => 'form-control','value'=>$cliente]);?>
                            </div>
                            <div class="form-group">
                                <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                            </div>
                        <?= $this->Form->end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--buscador-->

    <!--tabla usuarios-->
    <div class="col-md-9 col-md-pull-3">
        <div class="box">
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Rut') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Apellido') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Cliente') ?></th>
                            <th scope="col" class="actions"><?= __('Acciones') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($contacts as $contact): ?>
                            <tr>
                                <td><?= $this->Number->format($contact->id) ?></td>
                                <td><?= h($contact->rut) ?></td>
                                <td><?= h($contact->name) ?></td>
                                <td><?= h($contact->last_name) ?></td>
                                <td><?= $contact->has('client') ? $this->Html->link($contact->client->name, ['controller' => 'Clients', 'action' => 'view', $contact->client->id]) : '' ?></td>
                                <td class="actions">
                                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $contact->id], ['class' => 'btn btn-success btn-sm']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $contact->id], ['class' => 'btn btn-info btn-sm']) ?>
                                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $contact->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contact->id), 'class' => 'btn btn-danger btn-sm']) ?>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <div class="box-footer clearfix">
                <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
                </ul>
            </div>
        </div>
    </div>
<!--tabla usuarios-->
</div>
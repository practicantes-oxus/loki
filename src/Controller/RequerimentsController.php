<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Requeriments Controller
 *
 * @property \App\Model\Table\RequerimentsTable $Requeriments
 *
 * @method \App\Model\Entity\Requeriment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class RequerimentsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $priorityofRequirement = $this->Requeriments->PriorityOfRequirement->find('list', ['limit' => 200]);
        $contacts = $this->Requeriments->Contacts->find('list', ['limit' => 200]);

        
         //codigo para poder filtrar datos de una instancia segun la cuenta logueada
         $instances = $this->Auth->user()['instances_id'];
         $data_search = $this->request->query;
         $query = $this->Requeriments->find('search', [
            'search' => $data_search])
         ->contain(['Contacts', 'Instances','priorityofrequirement', 'RequerimentsTypes']);

         /*con esta linea de codigo nueva se quitar el filtro por instancias al super admin
              pero primero hay que definir la funcion getCurrentdUser en el app controller
            */
            if ($this->getCurrentUser()['roles_id'] !== 4) {
                $query->where(['Requeriments.instances_id'=>$instances]);
            }
         
         $this->paginate = [
             'limit' => 5
         ];
         $requeriments = $this->paginate($query);
 
         $titleForLayout= 'Requerimientos';
         $breadCrumb= 'Inicio';
         $this->set(compact('requeriments', 'data_search', 'priorityofRequirement', 'breadCrumb','contacts', 'titleForLayout'));
    }

    /**
     * View method
     *
     * @param string|null $id Requeriment id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $requeriment = $this->Requeriments->get($id, [
            'contain' => [
                'Contacts', 'RequerimentsTypes', 'Chores', 'priorityofrequirement', 'workOrders']
        ]);
        
        $titleForLayout= 'Requerimientos';
        $breadCrumb= 'Vista';
        $this->set(compact('users', 'titleForLayout', 'breadCrumb'));

        
        $this->paginate($this->Requeriments);
        $this->set('requeriment', $requeriment);
        $this->Requeriments->find();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $requeriment = $this->Requeriments->newEntity();
        if ($this->request->is('post')) {
            

            $requeriment = $this->Requeriments->patchEntity($requeriment, $this->request->getData());
            
            $requeriment->prices = true;
            if ($this->Requeriments->save($requeriment)) 
            {
                $this->Flash->success(__('El requerimiento a sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El requerimiento no a sido guardado. Intentelo nuevamente.'));
        }
        $contacts = $this->Requeriments->Contacts->find('list', ['limit' => 200]);
        $requerimentsTypes = $this->Requeriments->RequerimentsTypes->find('list', ['limit' => 200]);
        $priorityofRequirement = $this->Requeriments->PriorityOfRequirement->find('list', ['limit' => 200]);
        $instances = $this->Requeriments->Instances->find('list', ['limit' => 200]);
        $titleForLayout= 'Requerimientos';
        $breadCrumb= 'Agregar';
        $this->set(compact('requeriment','titleForLayout', 'contacts', 'breadCrumb', 'requerimentsTypes', 'priorityofRequirement', 'instances'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Requeriment id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $requeriment = $this->Requeriments->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $requeriment = $this->Requeriments->patchEntity($requeriment, $this->request->getData());
            if ($this->Requeriments->save($requeriment)) {
                $this->Flash->success(__('El requerimiento a sido editado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El requerimiento no a sido editado. Intentelo nuevamente.'));
        }
        $contacts = $this->Requeriments->Contacts->find('list', ['limit' => 200]);
        $requerimentsTypes = $this->Requeriments->RequerimentsTypes->find('list', ['limit' => 200]);
        $priorityofRequirement = $this->Requeriments->PriorityOfRequirement->find('list', ['limit' => 200]);
        $titleForLayout= 'Requerimientos';
        $breadCrumb= 'Editar';
        $this->set(compact('requeriment', 'breadCrumb', 'contacts', 'requerimentsTypes', 'priorityofRequirement', 'titleForLayout'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Requeriment id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $requeriment = $this->Requeriments->get($id);
        if ($this->Requeriments->delete($requeriment)) {
            $this->Flash->success(__('El requerimiento a sido eliminado'));
        } else {
            $this->Flash->error(__('El requerimiento no a sido eliminado. Intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

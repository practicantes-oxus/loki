<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\TypeEnterpry $typeEnterpry
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Type Enterpry'), ['action' => 'edit', $typeEnterpry->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Type Enterpry'), ['action' => 'delete', $typeEnterpry->id], ['confirm' => __('Are you sure you want to delete # {0}?', $typeEnterpry->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Type Enterpries'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Type Enterpry'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="typeEnterpries view large-9 medium-8 columns content">
    <h3><?= h($typeEnterpry->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($typeEnterpry->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($typeEnterpry->id) ?></td>
        </tr>
    </table>
</div>

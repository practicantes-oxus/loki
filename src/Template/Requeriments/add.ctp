<div class="row">
    <div class="col-md-3 col-md-push-9">
        <div class="box box-info"><!-- Acciones -->
            <div class="box-header with-border">
                <h3 class="box-title">Acciones</h3>
            </div>
    <?= $this->Form->create($requeriment) ?>
        <div class="box-body">
            <div class="row">
                <div class="col-xs-12">
                    <div class="btn-toolbar">                    
                        <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'Requeriments', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                        <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>
                    </div>                                        
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-md-9 col-md-pull-3">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Agregar Requerimiento</h3>
        </div>                            
    <div class="box-body">
        <div class="row"><!--fila 1 -->
            <div class="col-md-4">
                <? echo $this->Form->control('name_requeriment', ['class' => 'form-control','label' => 'Nombre del requerimiento']);?>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <? echo $this->Form->control('contacts_id', ['options' => $contacts, 'class' => 'form-control', 'empty' => 'Seleccionar','label' => 'Contacto']);?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <? echo $this->Form->control('requirement_type_id', ['options' => $requerimentsTypes, 'class' => 'form-control', 'empty' => 'Seleccionar','label' => 'Tipo de requerimiento']);?>
                </div>
            </div>
        </div>
        <br>
        <div class="row"> <!--fila 2 -->
            <div class="col-md-4">
                <? echo $this->Form->control('priority_id', ['options' => $priorityofRequirement, 'class' => 'form-control', 'empty' => 'Seleccionar','label' => 'Prioridad']);?>
            </div>
            <div class="col-md-4">
                <? echo $this->Form->control('prices', ['class' => 'form-control','label' => 'Cotización']);?>    
            </div>
            <div class="col-md-4">
                <? echo $this->Form->control('date', ['class' => 'form-control','label' => 'fecha']);?>
            </div>
        </div>    
        <br>
        <div class="row"><!-- fila 3 -->
            <div class="col-md-4">
                <div class="form-group">
                   <? echo $this->Form->control('description', ['class' =>'form-control', 'rows' => '5', 'cols' => '7','label' => 'Descripción']);?>
                </div>
            </div>
            <div class="col-md-4">
                <? echo $this->Form->control('instances_id', ['options' => $instances, 'class' => 'form-control','label' => 'Instancia']);?>
            </div>
        </div>
    </div>
    <?= $this->Form->end() ?>
</div>
 

<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Intancia Controller
 *
 * @property \App\Model\Table\IntanciaTable $Intancia
 *
 * @method \App\Model\Entity\Intancium[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class IntanciaController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $intancia = $this->paginate($this->Intancia);

        $this->set(compact('intancia'));
    }

    /**
     * View method
     *
     * @param string|null $id Intancium id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $intancium = $this->Intancia->get($id, [
            'contain' => []
        ]);

        $this->set('intancium', $intancium);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $intancium = $this->Intancia->newEntity();
        if ($this->request->is('post')) {
            $intancium = $this->Intancia->patchEntity($intancium, $this->request->getData());
            if ($this->Intancia->save($intancium)) {
                $this->Flash->success(__('The intancium has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The intancium could not be saved. Please, try again.'));
        }
        $this->set(compact('intancium'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Intancium id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $intancium = $this->Intancia->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $intancium = $this->Intancia->patchEntity($intancium, $this->request->getData());
            if ($this->Intancia->save($intancium)) {
                $this->Flash->success(__('The intancium has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The intancium could not be saved. Please, try again.'));
        }
        $this->set(compact('intancium'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Intancium id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $intancium = $this->Intancia->get($id);
        if ($this->Intancia->delete($intancium)) {
            $this->Flash->success(__('The intancium has been deleted.'));
        } else {
            $this->Flash->error(__('The intancium could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

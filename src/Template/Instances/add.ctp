<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Instance $instance
 */
?>

<!--div class="instances form large-9 medium-8 columns content">
    <?= $this->Form->create($instance) ?>
    <fieldset>
        <legend><?= __('Add Instance') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('rut_enterprises');
            echo $this->Form->control('email');
            echo $this->Form->control('phone');
            echo $this->Form->control('address');
            echo $this->Form->control('type_instances');
            echo $this->Form->control('type_turn');
            echo $this->Form->control('contacts');
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div-->



<div class="row">
    <div class="col-md-3 col-md-push-9">
        <div class="box box-info"><!-- Acciones -->
            <div class="box-header with-border">
                <h3 class="box-title">Acciones</h3>
            </div>
                <?= $this->Form->create($instance) ?>
            <div class="box-body">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="btn-toolbar">
                        <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'instances', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                    <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="col-md-9 col-md-pull-3">
    <div class="box box-info">
        <div class="box-header with-border">
                <h3 class="box-title">Agregar Instancia</h3>
        </div>  
        <div class="box-body">
            <div class="row"><!--fila 1 -->
                <div class="col-md-4">
                    <? echo $this->Form->control('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('rut_enterprises', ['class' => 'form-control', 'label' => 'Rut de la empresa']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('email', ['class' => 'form-control', 'label' => 'Email']);?>
                </div>
        </div>
    <br>
        <div class="row"> <!--fila 2 -->
                <div class="col-md-4">
                    <? echo $this->Form->control('phone', ['class' => 'form-control', 'label' => 'Telefono']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('address', ['class' => 'form-control', 'label' => 'Dirección']);?>
                </div>
                <!-- <div class="col-md-4">
                    <? echo $this->Form->control('type_turn', ['class' => 'form-control', 'label' => 'Descripción']);?>
                </div> -->
        </div>    
    <br>
        <div class="row"><!-- fila 3 -->
                <div class="col-md-4">
                    <? echo $this->Form->control('type_instances', ['class' => 'form-control','label' => 'Tipo de instancia']);?>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <? echo $this->Form->control('contacts', ['class' => 'form-control','label' => 'Contactos']);?>
                    </div>
                </div>
                <div class="col-md-4 col-precio">
                    <? echo $this->Form->control('date', ['class' => 'form-control','label' => 'Fecha']);?>
                </div>
        </div>
    </div>
</div>
        <?= $this->Form->end() ?>
    
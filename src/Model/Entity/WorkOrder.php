<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * WorkOrder Entity
 *
 * @property int $id
 * @property string $name
 * @property int $requeriments_id
 * @property int $work_order_status_id
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \App\Model\Entity\Requeriment $requeriment
 * @property \App\Model\Entity\WorkOrderStatus $work_order_status
 */
class WorkOrder extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}

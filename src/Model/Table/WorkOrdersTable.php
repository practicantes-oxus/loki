<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * WorkOrders Model
 *
 * @property \App\Model\Table\RequerimentsTable|\Cake\ORM\Association\BelongsTo $Requeriments
 * @property \App\Model\Table\WorkOrderStatusTable|\Cake\ORM\Association\BelongsTo $WorkOrderStatus
 *
 * @method \App\Model\Entity\WorkOrder get($primaryKey, $options = [])
 * @method \App\Model\Entity\WorkOrder newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\WorkOrder[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\WorkOrder|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WorkOrder|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\WorkOrder patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\WorkOrder[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\WorkOrder findOrCreate($search, callable $callback = null, $options = [])
 */
class WorkOrdersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('work_orders');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Requeriments', [
            'foreignKey' => 'requeriments_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('WorkOrderStatus', [
            'foreignKey' => 'work_order_status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Instances', [
            'foreignKey' => 'instances_id',
            'joinType' => 'INNER'
        ]);
        //codigo para mostrar datos de otra tabla 
        $this->hasMany('chores',[
            'foreignKey' => 'work_orders_id', 
        ]);

         // Add the behaviour to your table
         $this->addBehavior('Search.Search');

         // Setup search filter using search manager
         $this->searchManager()
             ->add('name', 'Search.Callback', [
                 'callback' => function($query, $args, $filter) {
                     $conditions = [
                         'WorkOrders.name =' => $args['name']
                     ];
                     
                     return $query->where($conditions);
                 }
             ])
             ->add('work_order_status_id', 'Search.Callback', [
                 'callback' => function($query, $args, $filter) {
                     $conditions = [
                         'WorkOrders.work_order_status_id' => $args['work_order_status_id']
                     ];
                     return $query->where($conditions);
                 }
             ])
             ->add('requeriments_id', 'Search.Callback', [
                 'callback' => function($query, $args, $filter) {
                     $conditions = [
                         'WorkOrders.requeriments_id' => $args['requeriments_id']
                     ];
                     return $query->where($conditions);
                 }
             ])
             ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        // $validator
        //     ->dateTime('date')
        //     ->requirePresence('date', 'create')
        //     ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['requeriments_id'], 'Requeriments'));
        $rules->add($rules->existsIn(['work_order_status_id'], 'WorkOrderStatus'));

        return $rules;
    }
}

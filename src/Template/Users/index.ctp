<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>

<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Inicio</a></li>
        <li><a href="#">Usuarios</a></li>
        <li class="active">Inicio de usuarios</li>
    </ol>
</section>
<div class="row">
    <!--buscador-->
    <div class="col-md-3 col-md-push-9">
        <div class="box box-default"><!-- filtro -->
            <div class="box-header with-border">
                <h3 class="box-title">Buscar</h3>
            </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <?= $this->Form->create() ?>
                                <div class="form-group">
                                    <?= $this->Form->input('rut', ['class' => 'form-control','label' => 'Rut']);?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->input('first_name', ['class' => 'form-control','label' => 'Nombre']);?>
                                </div>
                                <div class="form-group">
                                    <?if(isset($data_search['instances_id'])){
                                        $instan = $data_search['instances_id'];
                                    }else{
                                     $instan = 0;
                                    }?>
                                    <?= $this->Form->control('instances_id', ['label' => 'Empresa', 'options' => $instances,'class' => 'form-control','value'=>$instan, 'empty' => 'Seleccionar']);?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                                </div>
                            <?= $this->Form->end() ?>
                        </div>
                  </div>
                </div>
              </div>
            </div>
            <!--buscador-->


            <!--tabla usuarios-->
<div class="col-md-9 col-md-pull-3">
    <div class="box">
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('Rut') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Apellido') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Instancia') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Rol') ?></th>
                        <th scope="col" class="actions"><?= __('Acción') ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= h($user->rut) ?></td>
                        <td><?= h($user->first_name) ?></td>
                        <td><?= h($user->last_name) ?></td>
                        <td><?= $user->instance->name ?></td>
                        <td><?= $user->role->name ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Ver '), ['action' => 'view', $user->id], ['class' => 'btn btn-success btn-sm']) ?>


                            <?
                                if ($current_user['roles_id'] == 1) {
                                    echo $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-info btn-sm']);
                                }
                              
                            ?>


                            <?
                                if ($current_user['roles_id'] == 1)  {
                                    echo $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id],  ['class' => 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                }
                            ?>
                            
                            <!-- super admin -->
                            <?
                                if ($current_user['roles_id'] == 4) {
                                    echo $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-info btn-sm']);
                                }
                              
                            ?>


                            <?
                                if ($current_user['roles_id'] == 4)  {
                                    echo $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id],  ['class' => 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
              </ul>
          </div>
     </div>
</div>
<!--tabla usuarios-->
    <div class="row">
        <div class="col-lg-4 col-xs-4">
              <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h4>Registro de Usuarios</h4>
                        <p>Aqui puedes agregar un <br> nuevo usuario a tu empresa</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="#" class="">
                    <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Nuevo Usuario ', ['controller'=>'Users', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h4>Requerimientos</h4>
                        <p>Aqui puedes puedes ver <br> todos tus requerimientos</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-address-book"></i>
                    </div>
                        <a href="#" class="small-box-footer">
                        <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Requerimientos ', ['controller'=>'requeriments', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>   
                </a>
            </div>
        </div>
        <div class="col-lg-4 col-xs-4">
            <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h4>Ordenes de Trabajo</h4>
                        <p>Aqui puedes puedes ver <br> todos tus Ordenes de Trabajo</p>
                    </div>
                    <div class="icon">
                        <i class="fas fa-tags"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                    <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Ordenes de Trabajo ', ['controller'=>'WorkOrders', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>   
                </a>
        </div>
    </div> 
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\WorkOrder[]|\Cake\Collection\CollectionInterface $workOrders
 */
?>


    <!-- Content Header (Page header) -->
    <section class="content-header">
  
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
      </ol>
    </section>


      <div class="row">
      <div class="row">
    
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h4>Nueva Ordern de Trabajo</h4>
              <p>Aqui puedes puedes<br> generar nuevas Ordenes de Trabajo</p>
            </div>
            <div class="icon">
            <i class="fas fa-plus fa-xs"></i><i class="fas fa-tag"></i>
            </div>
            <a href="#" class="small-box-footer">
            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Ordenes de Trabajo ', ['controller'=>'WorkOrders', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

               
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-blue">
            <div class="inner">
              <h4>Requerimientos</h4>
              <p>Aqui puedes puedes ver <br> todos tus Requerimientos</p>
            </div>
            <div class="icon">
              <i class="fas fa-address-book"></i>
            </div>
            <a href="#" class="small-box-footer">
            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Requerimientos', ['controller'=>'WorkOrders', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

               
            </a>
          </div>
        </div>
        <div class="col-lg-4 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h4>Asignar tareas</h4>
              <p>Aqui puedes asignar y crear <br> tareas para las odenes de trabajo</p>
            </div>
            <div class="icon">
            <i class="fas fa-tasks"></i>
            </div>
            <a href="#" class="small-box-footer">
            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Asignar Tareas ', ['controller'=>'chores', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

               
            </a>
          </div>
        </div>

    </div>
<div class="col-md-3 col-md-push-9">
  <div class="box box-default"><!-- filtro -->
      <div class="box-header with-border">
          <h3 class="box-title">Buscar</h3>
      </div>
          <div class="box-body">
              <div class="row">
                  <div class="col-md-12">
                  <?= $this->Form->create() ?>
                                <div class="form-group">
                                    <?= $this->Form->input('name', ['class' => 'form-control','label' => 'Nombre']);?>
                                </div>
                                <div class="form-group">
                                    <?if(isset($data_search['work_order_status_id'])){
                                        $instans = $data_search['work_order_status_id'];
                                    }else{
                                     $instans = 0;
                                    }?>
                                    <?= $this->Form->input('requeriments_id', ['class' => 'form-control', 'options' => $requeriments,'label' => 'Requerimiento','value'=>$instans, 'empty' => 'Seleccionar']);?>
                                </div>
                                <div class="form-group">
                                    <?if(isset($data_search['work_order_status_id'])){
                                        $instan = $data_search['work_order_status_id'];
                                    }else{
                                     $instan = 0;
                                    }?>
                                    <?= $this->Form->control('work_order_status_id', ['label' => 'Estado de la orden', 'options' => $workOrderStatus,'class' => 'form-control','value'=>$instan, 'empty' => 'Seleccionar']);?>
                                </div>
                                <div class="form-group">
                                    <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                                </div>
                            <?= $this->Form->end() ?>
                  </div>
            </div>
          </div>
        </div>
      </div>
      <!--buscador-->
        <div class="col-md-9 col-md-pull-3">
          <div class="box">
            <div class="box-header with-border">
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table class="table table-bordered">
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Requerimientos') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Estado') ?></th>
                <th scope="col"><?= $this->Paginator->sort('Fecha') ?></th>
                <th scope="col" class="actions"><?= __('Acción') ?></th>
            </tr>
            <?php foreach ($workOrders as $workOrder): ?>
            <tr>
                <td><?= $this->Number->format($workOrder->id) ?></td>
                <td><?= h($workOrder->name) ?></td>
                <td><?= $workOrder->has('requeriments_id') ? $this->Html->link($workOrder->requeriment->name_requeriment, ['controller' => 'Requeriments', 'action' => 'view', $workOrder->requeriment->name]) : '' ?></td>
                <td><?= $workOrder->has('work_order_status') ? $this->Html->link($workOrder->work_order_status->name, ['controller' => 'WorkOrders', 'action' => 'edit', $workOrder->work_order_status->id]) : '' ?></td>
                <td><?= h($workOrder->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('Ver'), ['action' => 'view', $workOrder->id], ['class' => 'btn btn-success btn-md']) ?>
                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $workOrder->id], ['class' => 'btn btn-info btn-md']) ?>
                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $workOrder->id], ['confirm' => __('Are you sure you want to delete # {0}?', $workOrder->id), 'class' => 'btn btn-danger btn-md']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
              </table>
            </div>
            <!-- /.box-body -->
           
          <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
              </ul>
          </div>
</div> 

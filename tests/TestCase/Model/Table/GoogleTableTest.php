<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\GoogleTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\GoogleTable Test Case
 */
class GoogleTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\GoogleTable
     */
    public $Google;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.google',
        'app.intances'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('Google') ? [] : ['className' => GoogleTable::class];
        $this->Google = TableRegistry::getTableLocator()->get('Google', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->Google);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

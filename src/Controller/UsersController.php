<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {   
            //codigo para poder filtrar datos de una instancia segun la cuenta logueada
            $instances = $this->getCurrentUser()['instances_id'];
            $data_search = $this->request->query;
            $query = $this->Users->find('search', [
                'search' => $data_search,
                'contain' => ['Instances', 'Roles']]);
            /*con esta linea de codigo nueva se quitar el filtro por instancias
              pero primero hay que definir la funcion getCurrentdUser en el app controller
            */
            if ($this->getCurrentUser()['roles_id'] !== 4) {
                $query->where(['Users.instances_id'=>$instances]);
            }
            
            $this->paginate = [
                'limit' => 5
            ];
        
        $users = $this->paginate($query);
        
        $instances = $this->Users->Instances->find('list', ['limit' => 200]);
        $data_search = $this->request->query;
        $query = $this->Users->find('search', [
            'search' => $data_search,
            'contain' => [
                'Instances', 'Roles'
            ]
        ]);
        
        // $this->paginate = [
        //     'contain' => ['Instances', 'Roles']
        // ];
        // $users = $this->paginate($this->Users);
        $titleForLayout= 'Usuarios';
        $breadCrumb= 'Inicio';

        $this->set(compact('users', 'instances','data_search', 'titleForLayout', 'breadCrumb'));
    }


    public function redirectUrl() {
        if ($this->Auth->user()) {
            switch ($this->Auth->user()['roles_id']) {
                case 1: 
                    $this->redirect(['controller' => 'Users', 'action' => 'home']);
                    break;
                case 2: 
                    $this->redirect(['controller' => 'Requeriments', 'action' => 'index']);
                    break;
                case 3:
                    $this->redirect(['controller' => 'Chores', 'action' => 'index']);
                    break;
                case 4:
                    $this->redirect(['controller' => 'Users', 'action' => 'home']);
                    break;
            }
        } 
        return $this->redirect(['action' => 'login']);
    }

    /**
     * View method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Instances', 'Roles', 'chores']
        ]);
        $this->paginate($this->Users);
        $titleForLayout= 'Usuarios';
        $breadCrumb= 'Vista';
        $this->set(compact('user', 'titleForLayout', 'breadCrumb'));
        $this->Users->find();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            //pr($this->request);
            $user = $this->Users->patchEntity($user, $this->request->getData());
            //pr($user); die;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario a sido guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no a sido guardado. Intentelo nuevamente.'));
        }
        $instances = $this->Users->Instances->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $titleForLayout= 'Usuarios';
        $breadCrumb= 'Agregar';
        $this->set(compact('user', 'instances', 'roles', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Instances', 'Roles']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('El usuario a sido editado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El usuario no a sido editado. Intentelo nuevamente.'));
        }
        $instances = $this->Users->Instances->find('list', ['limit' => 200]);
        $roles = $this->Users->Roles->find('list', ['limit' => 200]);
        $titleForLayout= 'Usuarios';
        $breadCrumb= 'Editar';
        $this->set(compact('user', 'instances', 'roles', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('El usuario a sido eliminado.'));
        } else {
            $this->Flash->error(__('El usuario no a sido eliminado. Intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    
    public function login()
    {
        if($this->request->is('post'))
        {
            $user = $this->Auth->identify();
            if($user)
            {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            else
            {
                $this->Flash->error('Datos son invalidos, por favor intente nuevamente');
            }
        }


        if ($this->Auth->user())
        {
            return $this->redirectUrl(['Controller' => 'Users', 'action' => 'home']);
        }

    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function home()
    {  
        //codigo para poder filtrar datos de una instancia segun la cuenta logueada
        $instances = $this->Auth->user()['instances_id'];
        $data_search = $this->request->query;
        $query = $this->Users->find('search', [
            'search' => $data_search])
        ->contain(['Instances', 'Roles']);
         /*con esta linea de codigo nueva se quitar el filtro por instancias
              pero primero hay que definir la funcion getCurrentdUser en el app controller
            */
            if ($this->getCurrentUser()['roles_id'] !== 4) {
                $query->where(['Users.instances_id'=>$instances]);
            }
        
        $this->paginate = [
            'limit' => 5
        ];
        
        $users = $this->paginate($query);
        
        $instances = $this->Users->Instances->find('list', ['limit' => 200]);
        $data_search = $this->request->query;
        $query = $this->Users->find('search', [
            'search' => $data_search,
            'contain' => [
                'Instances', 'Roles'
            ]
        ]);
        
        // $this->paginate = [
        //     'contain' => ['Instances', 'Roles']
        // ];
        // $users = $this->paginate($this->Users);
        $titleForLayout= 'Usuarios';
        $breadCrumb= 'Inicio';

        $this->set(compact('users', 'instances','data_search', 'titleForLayout', 'breadCrumb'));
    }
}

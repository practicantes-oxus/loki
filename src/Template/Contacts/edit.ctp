
        
        
        
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Usuarios</a></li>
        <li class="active">Editar</li>
    </ol>
</section>
<div class="row">
    <div class="col-md-3 col-md-push-9">
        <div class="box box-info"><!-- Acciones -->
            <div class="box-header with-border">
                <h3 class="box-title">Acciones</h3>
            </div>
            <?= $this->Form->create($contact) ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="btn-toolbar">
                                <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'contacts', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                                <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>
                            </div>                                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-md-pull-3">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Editar Contacto</h3>
                </div>                            
                <div class="box-body">
                    <div class="row"><!--fila 1 -->
                        <div class="col-md-4">
                            <? echo $this->Form->control('rut', ['class' => 'form-control', 'label' => 'Rut']);?>
                        </div>
                        <div class="col-md-4">
                            <? echo $this->Form->control('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                        </div>
                        <div class="col-md-4">
                            <? echo $this->Form->control('last_name', ['class' => 'form-control', 'label' => 'Apellido']);?>
                        </div>
                    </div>
                    <br>
                    <div class="row"> <!--fila 2 -->
                        <div class="col-md-4">
                            <? echo $this->Form->control('phone', ['class' => 'form-control', 'label' => 'Telefono']);?>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <? echo $this->Form->control('clients_id', ['label' => 'Clientes', 'options' => $clients,'class' => 'form-control', 'empty' => 'Seleccionar']);?>
                            </div>
                        </div>
                    </div>    
                    <br>
                    <div class="row"><!-- fila 3 -->                                
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div> 
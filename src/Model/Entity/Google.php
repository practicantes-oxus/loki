<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Google Entity
 *
 * @property int $id
 * @property int $intances_id
 * @property string $CLIENT_ID
 * @property string $CLIENT_SECRET
 * @property string $REFRESH_TOKEN
 * @property string $SCOPES
 *
 * @property \App\Model\Entity\Intance $intance
 */
class Google extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'intance_id' => true,
        'CLIENT_ID' => true,
        'CLIENT_SECRET' => true,
        'REFRESH_TOKEN' => true,
        'SCOPES' => true,
        'intance' => true
    ];
}

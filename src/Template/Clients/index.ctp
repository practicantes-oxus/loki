<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client[]|\Cake\Collection\CollectionInterface $clients
 */
?>




   
<section class="content-header">
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Tables</a></li>
        <li class="active">Simple</li>
    </ol>
</section>
<div class="row">
<div class="container">
            <div class="row">
                <div class="col-lg-4 col-xs-6">
                    <!-- small box -->
                    <div class="small-box bg-green">
                        <div class="inner">
                            <h4>Registro de clientes</h4>
                            <p>Aqui puedes agregar un <br> nuevo Cliente</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="">
                            <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Nuevo Cliente ', ['controller'=>'Clients', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
                        </a>
                    </div>
                </div>
            </div>
       </div>
    <div class="col-md-9">
      <div class="box">
        <div class="box-header with-border">        
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('rut') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Dirección') ?></th>
                        <th scope="col" class="actions"><?= __('Acciones') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($clients as $client): ?>
                        <tr>
                            <td><?= $this->Number->format($client->id) ?></td>
                            <td><?= h($client->rut) ?></td>
                            <td><?= h($client->name) ?></td>
                            <td><?= h($client->email) ?></td>
                            <td><?= h($client->address)?></td>
                            <td class="actions">
                                <?= $this->Html->link(__('Ver '), ['action' => 'view', $client->id], ['class' => 'btn btn-success btn-sm']) ?>
                                <?= $this->Html->link(__('Editar'), ['action' => 'edit', $client->id], ['class' => 'btn btn-info btn-sm']) ?>
                                <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $client->id],  ['class' => 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $client->id)]) ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguiente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
              </ul>
          </div>
        </div>
        <!-- /.box-body -->
            
        <!-- /.box -->
    </div>
</div>
<!-- small box -->          
<div class="col-md-3">
    <div class="box box-default"><!-- filtro -->
        <div class="box-header with-border">
            <h3 class="box-title">Buscar</h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-12">
                    <?= $this->Form->create() ?>
                        <div class="form-group">
                            <?= $this->Form->input('rut', ['class' => 'form-control']);?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->input('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->control('email', ['label' => 'Email', 'class' => 'form-control', 'label' => 'Email']);?>
                        </div>
                        <div class="form-group">
                            <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                        </div>
                    <?= $this->Form->end() ?>
                </div>
                <!-- /.col -->
              </div>
            </div>
        </div>
    </div>
        <!--buscador-->
</div>
</div>

<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Chores Model
 *
 * @property \App\Model\Table\UsersTable|\Cake\ORM\Association\BelongsTo $Users
 * @property \App\Model\Table\WorkOrdersTable|\Cake\ORM\Association\BelongsTo $WorkOrders
 *
 * @method \App\Model\Entity\Chore get($primaryKey, $options = [])
 * @method \App\Model\Entity\Chore newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Chore[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Chore|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chore|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Chore patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Chore[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Chore findOrCreate($search, callable $callback = null, $options = [])
 */
class ChoresTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('chores');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsToMany('Requeriments');
        $this->belongsTo('Users', [
            'foreignKey' => 'users_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('WorkOrders', [
            'foreignKey' => 'work_orders_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('ChoresStatus', [
            'foreignKey' => 'chores_status_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Instances', [
            'foreignKey' => 'instances_id',
            'joinType' => 'INNER'
        ]);

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->add('work_orders_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Chores.work_orders_id' => $args['work_orders_id']
                    ];
                    return $query->where($conditions);
                }
            ])
            ->add('chores_status_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Chores.chores_status_id' => $args['chores_status_id']
                    ];
                    return $query->where($conditions);
                }
            ])
            ->add('users_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Chores.users_id' => $args['users_id']
                    ];
                    return $query->where($conditions);
                }
            ])
            ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('users_id')
            ->maxLength('users_id', 60)
            ->requirePresence('users_id', 'create')
            ->notEmpty('users_id');

        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        // $validator
        //     ->dateTime('date')
        //     ->requirePresence('date', 'create')
        //     ->notEmpty('date');
            

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['users_id'], 'Users'));
        $rules->add($rules->existsIn(['work_orders_id'], 'WorkOrders'));

        return $rules;
    }
}

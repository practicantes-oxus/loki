<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Google Model
 *
 * @property \App\Model\Table\IntancesTable|\Cake\ORM\Association\BelongsTo $Intances
 *
 * @method \App\Model\Entity\Google get($primaryKey, $options = [])
 * @method \App\Model\Entity\Google newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Google[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Google|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Google|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Google patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Google[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Google findOrCreate($search, callable $callback = null, $options = [])
 */
class GoogleTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('google');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->belongsTo('Intances', [
            'foreignKey' => 'intances_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('CLIENT_ID')
            ->maxLength('CLIENT_ID', 200)
            ->requirePresence('CLIENT_ID', 'create')
            ->notEmpty('CLIENT_ID');

        $validator
            ->scalar('CLIENT_SECRET')
            ->maxLength('CLIENT_SECRET', 200)
            ->requirePresence('CLIENT_SECRET', 'create')
            ->notEmpty('CLIENT_SECRET');

        $validator
            ->scalar('REFRESH_TOKEN')
            ->maxLength('REFRESH_TOKEN', 200)
            ->requirePresence('REFRESH_TOKEN', 'create')
            ->notEmpty('REFRESH_TOKEN');

        $validator
            ->scalar('SCOPES')
            ->requirePresence('SCOPES', 'create')
            ->notEmpty('SCOPES');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['intances_id'], 'Intances'));

        return $rules;
    }
}

<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TypeEnterpriesTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TypeEnterpriesTable Test Case
 */
class TypeEnterpriesTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\TypeEnterpriesTable
     */
    public $TypeEnterpries;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.type_enterpries'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('TypeEnterpries') ? [] : ['className' => TypeEnterpriesTable::class];
        $this->TypeEnterpries = TableRegistry::getTableLocator()->get('TypeEnterpries', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->TypeEnterpries);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}

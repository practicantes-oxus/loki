<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Intancia Model
 *
 * @method \App\Model\Entity\Intancium get($primaryKey, $options = [])
 * @method \App\Model\Entity\Intancium newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Intancium[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Intancium|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Intancium|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Intancium patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Intancium[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Intancium findOrCreate($search, callable $callback = null, $options = [])
 */
class IntanciaTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('intancia');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('rut')
            ->maxLength('rut', 60)
            ->requirePresence('rut', 'create')
            ->notEmpty('rut');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->email('email')
            ->requirePresence('email', 'create')
            ->notEmpty('email');

        $validator
            ->scalar('phone')
            ->maxLength('phone', 60)
            ->requirePresence('phone', 'create')
            ->notEmpty('phone');

        $validator
            ->scalar('gyre')
            ->maxLength('gyre', 60)
            ->requirePresence('gyre', 'create')
            ->notEmpty('gyre');

        $validator
            ->scalar('addres')
            ->maxLength('addres', 60)
            ->requirePresence('addres', 'create')
            ->notEmpty('addres');

        $validator
            ->integer('type_enterprice')
            ->requirePresence('type_enterprice', 'create')
            ->notEmpty('type_enterprice');

        $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));

        return $rules;
    }
}

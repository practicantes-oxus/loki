<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Instances Model
 *
 * @method \App\Model\Entity\Instance get($primaryKey, $options = [])
 * @method \App\Model\Entity\Instance newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Instance[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Instance|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Instance|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Instance patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Instance[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Instance findOrCreate($search, callable $callback = null, $options = [])
 */
class InstancesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('instances');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Type_instances', [
            'foreignKey' => 'type_instances',
            'joinType' => 'INNER'
        ]);
        $this->hasMany('Users',[
            'foreignKey' => 'instances_id'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 30)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        // $validator
        //     ->scalar('rut_enterprises')
        //     ->maxLength('rut_enterprises', 40)
        //     ->requirePresence('rut_enterprises', 'create')
        //     ->notEmpty('rut_enterprises');

        // $validator
        //     ->email('email')
        //     ->requirePresence('email', 'create')
        //     ->notEmpty('email');

        // $validator
        //     ->scalar('phone')
        //     ->maxLength('phone', 40)
        //     ->requirePresence('phone', 'create')
        //     ->notEmpty('phone');

        // $validator
        //     ->scalar('address')
        //     ->maxLength('address', 30)
        //     ->requirePresence('address', 'create')
        //     ->notEmpty('address');

       /* $validator
            ->integer('type_enterprises')
            ->requirePresence('type_enterprises', 'create')
            ->notEmpty('type_enterprises');

        $validator
            ->integer('type_turn')
            ->requirePresence('type_turn', 'create')
            ->notEmpty('type_turn');*/

        $validator
            ->scalar('contacts')
            ->maxLength('contacts', 30)
            ->requirePresence('contacts', 'create')
            ->notEmpty('contacts');

       /* $validator
            ->dateTime('date')
            ->requirePresence('date', 'create')
            ->notEmpty('date');*/

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->isUnique(['type_instances'], 'Type_instances'));

        return $rules;
    }
}

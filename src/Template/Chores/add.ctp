<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Chore $chore
 */
?>

<!--div class="chores form large-9 medium-8 columns content">
    <?= $this->Form->create($chore) ?>
    <fieldset>
        <legend><?= __('Add Chore') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('description');
            echo $this->Form->control('users_id', ['options' => $users]);
            echo $this->Form->control('work_orders_id', ['options' => $workOrders]);
            echo $this->Form->control('chores_status_id', ['options' => $choresStatus]);
            echo $this->Form->control('date');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div-->



  <section class="content-header">
                    <ol class="breadcrumb">
                        <li><a href="#"><i class="fa fa-dashboard"></i> Nivel</a></li>
                        <li class="active">Aquí</li>
                    </ol>
                </section>
                    <div class="row">
                        <div class="col-md-3 col-md-push-9">

                            <div class="box box-info"><!-- Acciones -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Acciones</h3>
                                </div>
                                <?= $this->Form->create($chore) ?>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-xs-12">
                                            <div class="btn-toolbar">
                                            <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'chores', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                                            <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

<div class="col-md-9 col-md-pull-3">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Agregar Tarea</h3>
        </div>
        <div class="box-body">
            <div class="row"><!--fila 1 -->
                <div class="col-md-4">
                    <? echo $this->Form->control('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('users_id', ['options' => $users,'class' => 'form-control', 'empty' => 'Seleccione', 'label' => 'Usuarios']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('work_orders_id', ['options' => $workOrders, 'class' => 'form-control', 'empty' => 'Seleccione', 'label' => 'Orden de trabajo']);?>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-4">
                    <? echo $this->Form->control('chores_status_id', ['options' => $choresStatus, 'class' => 'form-control', 'empty' => 'Seleccione', 'label' => 'Estado de tarea']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('date', ['class' => 'form-control', 'label' => 'Fecha']);?>
                </div>
                <div class="col-md-4">
                    <div classs="form-group">
                        <? echo $this->Form->control('description', ['class' => 'form-control', 'label' => 'Descripción']);?>
                    </div>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('instances_id', ['options' => $instances, 'class' => 'form-control', 'label' => 'Instancia']);?>
                </div>
                <div class="col-md-4">
                    <? echo $this->Form->control('requeriments_id', ['options' => $requeriment, 'class' => 'form-control', 'label' => 'Requerimiento']);?>
                </div>
            </div>
            <br>
        </div>
        <?= $this->Form->end() ?>
    </div>
</div> 
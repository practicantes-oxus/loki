<header class="main-header">

    <a href="#" class="logo no-padding">
    <span class="logo-mini"><b>LK</b><b class="color-red"></b></span>
        <span class="logo-lg"><b>Loki Proyect</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <div class="image pull-left div">
                            <style>
                                .pd{
                                    margin-left: -25px;
                                }
                                .div{
                                    padding-left: 30px;
                                }
                            </style> 
                            <i class="fas fa-user-circle fa-lg pd"></i>
                        </div>
                        <span class="hidden-xs"><?= $current_user['first_name'].' '.$current_user['last_name']?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <li class="user-header">
                            <p>
                                <?= $current_user['first_name'].' '.$current_user['last_name']?>
                            </p>

                            
                           <h5><i class="fas fa-user-circle fa-5x"></i></h5>
                            <p>
                                <small><?= $current_user['instances_id']?></small>
                                <small><?= $current_user['email']?></small>
                            </p>
                        </li>
                        
                        </li>
                        <li class="user-footer">
                            <div class="pull-right">
                                <?= $this->Html->link('Cerrar Sesion', ['controller' => 'users', 'action' => 'logout'], ['class' => 'btn btn-default'])?>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>

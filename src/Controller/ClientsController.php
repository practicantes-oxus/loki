<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Clients Controller
 *
 * @property \App\Model\Table\ClientsTable $Clients
 *
 * @method \App\Model\Entity\Client[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ClientsController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $data_search = $this->request->query;
        $query = $this->Clients->find('search', [
            'search' => $data_search,
            'contain' => [
                'Banks', 'Regions'
            ]
        ]);

        // $this->paginate = [
        //     'contain' => ['Banks', 'Regions']
        // ];
        $clients = $this->paginate($query);

        $titleForLayout= 'Clientes';
        $breadCrumb= 'Inicio';
        $this->set(compact('clients', 'data_search', 'instances', 'titleForLayout', 'breadCrumb'));

        
        //codigo para poder filtrar datos de una instancia segun la cuenta logueada
        $instances = $this->Auth->user()['instances_id'];

        $query = $this->Clients->find()
        ->where(['Clients.instances_id'=>$instances])
        ->contain(['Instances', 'Banks', 'Regions']);
        
        $this->paginate = [
            'limit' => 5
        ];
        $clients = $this->paginate($query);
    }

    /**
     * View method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => ['Banks', 'Regions', 'Contacts']
        ]);
        $this->paginate($this->Clients);
        $titleForLayout= 'Clientes';
        $breadCrumb= 'Vista';
        $this->set(compact('client', $client, 'titleForLayout', 'breadCrumb'));
        $this->Clients->find();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $client = $this->Clients->newEntity();
        if ($this->request->is('post')) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            //pr($client); die;
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('El cliente se a guardado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El cliente no pudo ser guardado. Intentelo nuevamente.'));
        }
        $banks = $this->Clients->Banks->find('list', ['limit' => 200]);
        $regions = $this->Clients->Regions->find('list', ['limit' => 200]);
        $instances = $this->Clients->Instances->find('list', ['limit' => 200]);
        $titleForLayout= 'Clientes';
        $breadCrumb= 'Agregar';
        $this->set(compact('client', 'banks', 'regions', 'instances', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $client = $this->Clients->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $client = $this->Clients->patchEntity($client, $this->request->getData());
            if ($this->Clients->save($client)) {
                $this->Flash->success(__('El cliente a sido editado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('El cliente no pudo editarce. Intentelo nuevamente.'));
        }
        $banks = $this->Clients->Banks->find('list', ['limit' => 200]);
        $regions = $this->Clients->Regions->find('list', ['limit' => 200]);
        $titleForLayout= 'Clientes';
        $breadCrumb= 'Editar';
        $this->set(compact('client', 'banks', 'regions', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Client id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $client = $this->Clients->get($id);
        if ($this->Clients->delete($client)) {
            $this->Flash->success(__('El cliente a sido eliminado.'));
        } else {
            $this->Flash->error(__('El cliente no pudo ser eliminado. Intentelo nuevamente'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

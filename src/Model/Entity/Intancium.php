<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Intancium Entity
 *
 * @property int $id
 * @property string $rut
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $gyre
 * @property string $addres
 * @property int $type_enterprice
 * @property \Cake\I18n\FrozenTime $date
 */
class Intancium extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'rut' => true,
        'name' => true,
        'email' => true,
        'phone' => true,
        'gyre' => true,
        'addres' => true,
        'type_enterprice' => true,
        'date' => true
    ];
}

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Enterprise[]|\Cake\Collection\CollectionInterface $enterprises
 */
?>

<div class="enterprises index large-9 medium-8 columns content">
    <h3><?= __('Enterprises') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('phone') ?></th>
                <th scope="col"><?= $this->Paginator->sort('date') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($enterprises as $enterprise): ?>
            <tr>
                <td><?= $this->Number->format($enterprise->id) ?></td>
                <td><?= h($enterprise->name) ?></td>
                <td><?= h($enterprise->email) ?></td>
                <td><?= h($enterprise->phone) ?></td>
                <td><?= h($enterprise->date) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $enterprise->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $enterprise->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $enterprise->id], ['confirm' => __('Are you sure you want to delete # {0}?', $enterprise->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

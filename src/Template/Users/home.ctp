<div class="row">
                        <div class="col-md-4 col-xs-12">
                            <div class="info-box">
                                <?= $this->Html->link('<span><i class="fa fa-file-code-o"></i></span>',['controller' => 'requeriments', 'action' => 'add'], ['class' => 'btn btn-xs bg-green info-box-icon bg-blue info-box', 'escape' => false])?>
                                <div class="info-box-content">
                                <span class="info-box-number">Agregar Requerimiento</span>
                                <span class="info-box-text">Descripción Genérica</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="info-box">
                            <?= $this->Html->link('<span><i class="fas fa-tasks"></i></span>',['controller' => 'chores', 'action' => 'add'], ['class' => 'btn btn-xs info-box-icon bg-aqua info-box', 'escape' => false])?>
                                <div class="info-box-content">
                                <span class="info-box-number">Agregar Tarea</span>
                                <span class="info-box-text">Descripción Genérica</span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-12">
                            <div class="info-box">
                                <?= $this->Html->link('<span><i class="fas fa-tags"></i></span>',['controller' => 'WorkOrders', 'action' => 'add'], ['class' => 'btn btn-xs info-box-icon bg-red info-box', 'escape' => false])?>
                                <div class="info-box-content">
                                <span class="info-box-number">Ordenes de Trabajo</span>
                                <span class="info-box-text">Descripción Genérica</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-3 col-md-push-9">
                            <div class="box box-default"><!-- filtro -->
                                <div class="box-header with-border">
                                    <h3 class="box-title">Buscar</h3>
                                </div>
                                <div class="box-body">
                                    <div class="row">
                                        <div class="col-md-12">
                                        <?= $this->Form->create() ?>
                                            <div class="form-group">
                                                <?= $this->Form->input('rut', ['class' => 'form-control','label' => 'Rut']);?>
                                            </div>
                                            <div class="form-group">
                                                <?= $this->Form->input('first_name', ['class' => 'form-control','label' => 'Nombre']);?>
                                            </div>
                                            <div class="form-group">
                                                    <?if(isset($data_search['instances_id'])){
                                                        $instan = $data_search['instances_id'];
                                                    }else{
                                                        $instan = 0;
                                                    }?>
                                                    <?= $this->Form->control('instances_id', ['label' => 'Empresa', 'options' => $instances,'class' => 'form-control','value'=>$instan, 'empty' => 'Seleccionar']);?>
                                            </div>
                                            <div class="form-group">
                                                <?= $this->Form->button(__('<i class="fa fa-search fa-side"></i> Buscar'), ['class' => 'btn btn-info']); ?>
                                            </div>
                                        <?= $this->Form->end() ?>
                                    </div>
                                    <!-- /.col -->
                                </div>
                            </div>
                        </div>
                    </div>
                        <div class="col-md-9 col-md-pull-3">
                        <div class="box box-default">
                            <div class="box-header with-border">
                                <h3 class="box-title">Listado Usuarios</h3>
                            </div>
                            <div class="box-body">
                                <div class="table-responsive">
                                    <table class="table table-condensed text-center">
                                    <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Rut') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Apellido') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Instancia') ?></th>
                        <th scope="col"><?= $this->Paginator->sort('Rol') ?></th>
                        <th scope="col" class="actions"><?= __('Acción') ?></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($users as $user): ?>
                    <tr>
                        <td><?= $this->Number->format($user->id) ?></td>
                        <td><?= h($user->rut) ?></td>
                        <td><?= h($user->first_name) ?></td>
                        <td><?= h($user->last_name) ?></td>
                        <td><?= h($user->instance->name) ?></td>
                        <td><?= h($user->role->name) ?></td>
                        <td class="actions">
                            <?= $this->Html->link(__('Ver '), ['action' => 'view', $user->id], ['class' => 'btn btn-success btn-sm']) ?>
                            
                            <?
                                if ($current_user['roles_id'] == 1) {
                                    echo $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-info btn-sm']);
                                }
                              
                            ?>
                            
                            <?
                                if ($current_user['roles_id'] == 1)  {
                                   echo $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id],  ['class' => 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                }
                            ?>
                            
                            <!-- super admin -->
                            <?
                                if ($current_user['roles_id'] == 4) {
                                    echo $this->Html->link(__('Editar'), ['action' => 'edit', $user->id], ['class' => 'btn btn-info btn-sm']);
                                }
                              
                            ?>


                            <?
                                if ($current_user['roles_id'] == 4)  {
                                    echo $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $user->id],  ['class' => 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $user->id)]); 
                                }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>

                </tbody>
                                    </table>

            <div class="box-footer clearfix">
              <ul class="pagination pagination-sm no-margin pull-right">
                    <?= $this->Paginator->first(' ' . __('Primera')) ?>
                    <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('Siguente') . ' ') ?>
                    <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
              </ul>
            </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </section>
            </div>
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    Cualquier cosa que quieras colocar
                </div>
                <strong>Copyright &copy; 2016 <a href="#">Compañia Genérica.</a></strong> Todos los derechos reservados.
            </footer>
            <div class="control-sidebar-bg"></div>
            <div id="modalContacto" tabindex="-1" role="dialog" class="modal fade in">
                <div role="document" class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-body">
                            <h3>Agregar contacto con el cliente</h4>
                            <div class="table-responsive">
                                <table class="table table-condensed">
                                    <thead>
                                        <th>Forma contacto</th>
                                        <th>Comentario</th>
                                        <th>Guardar</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <select class="form-control input-sm" name="">
                                                    <option value="" class="">forma 1</option>
                                                    <option value="" class="">forma 2</option>
                                                    <option value="" class="">forma 3</option>
                                                    <option value="" class="">forma 4</option>
                                                    <option value="" class="">forma 5</option>
                                                </select>
                                            </td>
                                            <td>
                                                <div class="form-group">
                                                    <input type="text" name="" value="" class="form-control input-sm">
                                                </div>
                                            </td>
                                            <td>
                                                <div class="box-tools width-auto">
                                                    <a href="#" class="btn btn-box-tool" title="Ver detalle"><i class="fa fa-save"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <h4>Registo de contactos con el cliente</h3>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover text-center">
                                    <thead>
                                        <th>Nombre</th>
                                        <th>Fecha</th>
                                        <th>Forma contacto</th>
                                        <th>Comentario</th>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><a href="#" class="drop-info" title="Ver detalles cliente">Nombre Genérico</a></td>
                                            <td>2017-10-5 13:1</td>
                                            <td>Correo electronico</td>
                                            <td>Texto Generico Texto Generico Texto Generico Texto Generico</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#" class="drop-info" title="Ver detalles cliente">Nombre Genérico</a></td>
                                            <td>2017-10-5 13:1</td>
                                            <td>Llamada</td>
                                            <td>Texto Generico Texto Generico Texto Generico Texto Generico</td>
                                        </tr>
                                        <tr>
                                            <td><a href="#" class="drop-info" title="Ver detalles cliente">Nombre Genérico</a></td>
                                            <td>2017-10-5 13:1</td>
                                            <td>Whatsapp</td>
                                            <td>Texto Generico Texto Generico Texto Generico Texto Generico</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Modal-->
        <div id="modal" tabindex="-1" role="dialog" class="modal fade in">
            <div role="document" class="modal-dialog">

                <div class="box box-info box-solid">
                    <div class="box-header with-border">
                        <h3 class="box-title">Modal Generico Cobranzas</h3>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label>Campo Genérico</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label>Campo Genérico</label>
                                <input type="text" class="form-control">
                            </div>
                            <div class="col-md-4">
                                <label>Campo Genérico</label>
                                <input type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="box-footer">
                    </div>
                </div>

            </div>
        </div>
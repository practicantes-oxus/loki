<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Instances Controller
 *
 * @property \App\Model\Table\InstancesTable $Instances
 *
 * @method \App\Model\Entity\Instance[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InstancesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $instances = $this->paginate($this->Instances);
        $titleForLayout= 'Instancias';
        $breadCrumb= 'Inicio';

        $this->set(compact('instances', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * View method
     *
     * @param string|null $id Instance id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $instance = $this->Instances->get($id, [
            'contain' => ['Users', 'Type_instances']
        ]);
        //pr($instance); die;
        $this->paginate($this->Instances);
        $titleForLayout= 'Instancias';
        $breadCrumb= 'Vista';

        $this->set(compact('instance', $instance, 'titleForLayout', 'breadCrumb'));
        $this->Instances->find();
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $instance = $this->Instances->newEntity();
        if ($this->request->is('post')) {
            $instance = $this->Instances->patchEntity($instance, $this->request->getData());
            if ($this->Instances->save($instance)) {
                $this->Flash->success(__('La intancia se a guardado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La intancia no pudo guardarce. Intentelo nuevamente.'));
        }
        $titleForLayout= 'Instancias';
        $breadCrumb= 'Agregar';
        $this->set(compact('instance', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Instance id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $instance = $this->Instances->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $instance = $this->Instances->patchEntity($instance, $this->request->getData());
            if ($this->Instances->save($instance)) {
                $this->Flash->success(__('La intancia se pudo editar correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La intancia no pudo editarce. Intentelo nuevamente.'));
        }
        $titleForLayout= 'Instancias';
        $breadCrumb= 'Editar';
        $this->set(compact('instance', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Instance id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $instance = $this->Instances->get($id);
        if ($this->Instances->delete($instance)) {
            $this->Flash->success(__('La intancia a sio eliminada correctamente.'));
        } else {
            $this->Flash->error(__('La intancia no pudo eliminarce. Intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

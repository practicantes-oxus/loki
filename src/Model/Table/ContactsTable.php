<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Contacts Model
 *
 * @property \App\Model\Table\ClientsTable|\Cake\ORM\Association\BelongsTo $Clients
 *
 * @method \App\Model\Entity\Contact get($primaryKey, $options = [])
 * @method \App\Model\Entity\Contact newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Contact[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Contact|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Contact patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Contact[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Contact findOrCreate($search, callable $callback = null, $options = [])
 */
class ContactsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);
        
        $this->setTable('contacts');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Clients', [
            'foreignKey' => 'clients_id'
        ]);
        $this->hasMany('Requeriments', [
            'foreignKey' => 'contacts_id',
        ]);
        $this->belongsTo('Instances', [
            'foreignkey' => 'instances_id'
        ]);

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->add('clients_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Contacts.clients_id =' => $args['clients_id']
                    ];
                    
                    return $query->where($conditions);
                }
            ])
            ->add('name', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Contacts.name' => $args['name']
                    ];
                    return $query->where($conditions);
                }
            ])
            ->add('rut', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Contacts.rut' => $args['rut']
                    ];
                    return $query->where($conditions);
                }
            ])
            ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('rut')
        //     ->maxLength('rut', 60)
        //     ->allowEmpty('rut');

        $validator
            ->scalar('name')
            ->maxLength('name', 20)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        $validator
            ->scalar('clients_id')
            ->maxLength('clients_id', 20)
            ->requirePresence('clients_id', 'create')
            ->notEmpty('clients_id');

        // $validator
        //     ->scalar('phone')
        //     ->maxLength('phone', 40)
        //     ->allowEmpty('phone');

        // $validator
        //     ->scalar('resource_name')
        //     ->maxLength('resource_name', 130)
        //     ->requirePresence('resource_name', 'create')
        //     ->notEmpty('resource_name');

        // $validator
        //     ->scalar('password')
        //     ->maxLength('password', 225)
        //     ->allowEmpty('password');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['clients_id'], 'Clients'));

        return $rules;
    }
}

<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Chore Entity
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $users_id
 * @property int $work_orders_id
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \App\Model\Entity\User $user
 * @property \App\Model\Entity\WorkOrder $work_order
 */
class Chore extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}

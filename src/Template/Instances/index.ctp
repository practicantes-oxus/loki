<div class="row">
<div class="row">
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-green">
            <div class="inner">
                <h4>Agrega Una Nueva Instancia</h4>
                <p>Aqui puedes agregar una <br> nueva instancia</p>
            </div>
            <div class="icon">
                <i class="far fa-building"></i>
            </div>
            <a href="#" class="">
                <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Agregar Nueva instancia ', ['controller'=>'instances', 'action'=>'add'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
            </a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-aqua">
            <div class="inner">
                <h4>Usuarios</h4>
                <p>Aqui puedes puedes ver <br> todos los Usuarios</p>
            </div>
            <div class="icon">
                <i class="far fa-user"></i>
            </div>
            <a href="#" class="small-box-footer">
                <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Usuarios ', ['controller'=>'requeriments', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>

            </a>
        </div>
    </div>
    <div class="col-lg-4 col-xs-4">
        <!-- small box -->
        <div class="small-box bg-red">
            <div class="inner">
                <h4>Ordenes de Trabajo</h4>
                <p>Aqui puedes puedes ver <br> todos tus Ordenes de Trabajo</p>
            </div>
            <div class="icon">
                <i class="fas fa-tags"></i>
            </div>
            <a href="#" class="small-box-footer">
                <?= $this->Html->link('<i class="fa fa-arrow-circle-right"></i> Ver Ordenes de Trabajo ', ['controller'=>'WorkOrders', 'action'=>'index'], ['escape'=>false, 'class' => 'small-box-footer']); ?>
            </a>
        </div>
    </div>
</div>
    <!--buscador-->
        <div class="col-md-3 col-md-push-9">
            <div class="box box-default"><!-- filtro -->
                <div class="box-header with-border">
                    <h3 class="box-title">Buscar</h3>
                </div>
                <div class="box-body">
                    
                </div>
            </div>
        </div>
    <!--buscador-->
    <!--tabla usuarios-->
        <div class="col-md-9 col-md-pull-3">
            <div class="row">
                <div class="box">
                <!-- /.box-header -->
                    <div class="box-body">
                        <table class="table table-hover">
                            <thead>
                                <tr>
                                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('Rut') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
                                    <th scope="col"><?= $this->Paginator->sort('Teléfono') ?></th>
                                    <th scope="col" class="actions"><?= __('Acción') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($instances as $instance): ?>
                                <tr>
                                    <td><?= $this->Number->format($instance->id) ?></td>
                                    <td><?= h($instance->name) ?></td>
                                    <td><?= h($instance->rut_enterprises) ?></td>
                                    <td><?= h($instance->email) ?></td>
                                    <td><?= h($instance->phone) ?></td>
                                    <td class="actions">
                                    <?= $this->Html->link(__('Detalles'), ['action' => 'view', $instance->id], ['class' => 'btn btn-success btn-sm']) ?>
                                    <?= $this->Html->link(__('Editar'), ['action' => 'edit', $instance->id], ['class' => 'btn btn-info btn-sm']) ?>
                                    <?= $this->Form->postLink(__('Eliminar'), ['action' => 'delete', $instance->id],  ['class' => 'btn btn-danger btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $instance->id)]) ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>    
    <!--tabla usuarios-->   


jQuery(document).ready(function($) {
    updateDocModal($('#clientDocument').val());
    $('#typeDocSave').on('change', function(event) {
        var documentTypeName = documentTypes[$(this).val()];
        var validationForCommentary = 0;
        var validationForName = 0;
        $.each( documentTypes, function(i, val) {
            if ($.trim($('#nameDocSave').val()) == val) {
                validationForName = 1;
            }
            if($.trim($('#commentaryDocSave').val()) == val) {
               validationForCommentary = 1; 
            }
        });
        if($.trim($('#nameDocSave').val()).length < 1 || validationForName == 1){
            $('#nameDocSave').val(documentTypeName);
        }
        if($.trim($('#commentaryDocSave').val()).length < 1 || validationForCommentary){
            $('#commentaryDocSave').val(documentTypeName);
        }
    });
    $('#formAddDocument').on('click', function(event){
        var validation = true;
        var name = $('#nameDocSave').val();
        var document_name;
        var commentary = $('#commentaryDocSave').val();
        var document_type_id = $('#typeDocSave').val();
        var client_id = $('#clientDocument').val();
        var expire_date = $('#date').val();
        if($('#documentDocSave').val()){
            document_name = $('#documentDocSave')[0].files[0];
        }else{
            document_name = "";
        }
        if(name.length < 1){
            validation = false;
            $('#divNameDocSave').focus();
            $('#divNameDocSave').addClass('has-error');
        }else{
            $('#divNameDocSave').removeClass('has-error');
        }
        if(commentary.length < 1){
            validation = false;
            $('#divCommentaryDocSave').focus();
            $('#divCommentaryDocSave').addClass('has-error');
        }else{
            $('#divCommentaryDocSave').removeClass('has-error');
        }
        if (document_type_id < 1){
            validation = false;
            $('#divTypeDocSave').focus();
            $('#divTypeDocSave').addClass('has-error');
        }else{
            $('#divTypeDocSave').removeClass('has-error');
        }
        if(validation){
            var formData = new FormData();
            formData.append("name",name);
            formData.append("document",document_name);
            formData.append("commentary",commentary);
            formData.append("document_type_id",document_type_id);
            formData.append("client_id",client_id);
            formData.append("expire_date",expire_date);
            $.ajax({
                url: '/ClientsDocuments/add',
                data: formData,
                type: 'post',
                cache: false,
                contentType: false,
                processData: false,
                beforeSend: function(){
                    swal({
                        type: "info",
                        title: "Espera!",
                        text: "Tu documento se esta subiendo",
                        button: false,
                        allowOutsideClick: false,
                        showConfirmButton: false,
                        allowEscapeKey: false,
                    });
                },
                success: function(response){
                    //swal('response',response,'info');
                    if(response == 1) {
                        swal("Muy Bien!", "Tu archivo se ha subido de forma correcta", "success");
                    } else {
                        swal("Error", "Ha ocurrido un error en el proceso de subir el archivo <br><b><u> Intente mas tarde </b></u>", "error");
                    }
                    updateDocModal($('#clientDocument').val());
                },
            });
        }else{
            swal("Porfavor", "Rellene todos los campos en rojo", "error");
        }
    });
});
function updateDocModal(id){
    $.ajax({
        url: '/ClientsDocuments/client-doc-modal',
        data: {'client_id':id},
        type: 'post',
        success: function(response){
            $('#DocModal').html(response);
            $('#DocModal').css('display','block');
        },
    });
}
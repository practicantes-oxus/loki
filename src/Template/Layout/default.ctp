<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'Loki Proyect';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>
        Proyecto Loki:
        <?= $this->fetch('title') ?>
    </title>
    <?= $this->Html->meta('icon') ?>

     <?= $this->Html->css([
             'bootstrap.min' ,
             'bootstrap.min.css',
             'skins/_all-skins.min',
             'font-awesome.min',
             'ionicons.min',
             'AdminLTE',
             'AdminLTE.min',
             'animate',
             'style.css',
        ]); ?>

        <?= $this->Html->script([
            'jquery-2.2.3.min',
            'jquery-ui.min',            
            'bootstrap.min',
            'moment',
            'bootstrap-select',
            'bootstrap-filestyle.min',
            'app.min',
            'adminlte.min',
            'demo',
            'fastclick',
            'jquery.slimscroll.min'
            //'tableSheets'
        ])?> 

        <!-- Icons -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
                <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
            <!-- Notify -->
                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-notify/0.2.0/css/bootstrap-notify.css">
            <!-- Google Font -->
                <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
            <!--Date time pricker-->
                <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.css"> 

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body class="hold-transition skin-blue sidebar-collapse sidebar-mini">
<?= $this->Element('header')?>

    <?= $this->Element('navbar')?>
    <div class="content-wrapper">
        <section class="content-header">
            <?= $this->Flash->render() ?>
            <h1>
            
            <?php echo $titleForLayout; ?>
                <small>Descripción opcional</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i><?php echo $titleForLayout; ?></a></li>
                <li class="active"><?php echo $breadCrumb?></li>
            </ol>
        </section>
        <section class="content">
            <?= $this->fetch('content') ?>
        </section>
    </div>    
    <footer>
    </footer>
</body>
</html>

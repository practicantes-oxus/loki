<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * ContactsLog Entity
 *
 * @property int $id
 * @property int $contacts_id
 * @property string $summary
 * @property int $contacts_type
 * @property int $requeriments_id
 * @property int $users_id
 *
 * @property \App\Model\Entity\Contact $contact
 * @property \App\Model\Entity\Requeriment $requeriment
 * @property \App\Model\Entity\User $user
 */
class ContactsLog extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'contacts_id' => true,
        'summary' => true,
        'contacts_type' => true,
        'requeriments_id' => true,
        'users_id' => true,
        'contact' => true,
        'requeriment' => true,
        'user' => true
    ];
}

<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsLog[]|\Cake\Collection\CollectionInterface $contactsLog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Contacts Log'), ['action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Requeriments'), ['controller' => 'Requeriments', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New Requeriment'), ['controller' => 'Requeriments', 'action' => 'add']) ?></li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?></li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?></li>
    </ul>
</nav>
<div class="contactsLog index large-9 medium-8 columns content">
    <h3><?= __('Contacts Log') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contacts_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('summary') ?></th>
                <th scope="col"><?= $this->Paginator->sort('contacts_type') ?></th>
                <th scope="col"><?= $this->Paginator->sort('requeriments_id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('users_id') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($contactsLog as $contactsLog): ?>
            <tr>
                <td><?= $this->Number->format($contactsLog->id) ?></td>
                <td><?= $contactsLog->has('contact') ? $this->Html->link($contactsLog->contact->name, ['controller' => 'Contacts', 'action' => 'view', $contactsLog->contact->id]) : '' ?></td>
                <td><?= h($contactsLog->summary) ?></td>
                <td><?= $this->Number->format($contactsLog->contacts_type) ?></td>
                <td><?= $contactsLog->has('requeriment') ? $this->Html->link($contactsLog->requeriment->id, ['controller' => 'Requeriments', 'action' => 'view', $contactsLog->requeriment->id]) : '' ?></td>
                <td><?= $contactsLog->has('user') ? $this->Html->link($contactsLog->user->name, ['controller' => 'Users', 'action' => 'view', $contactsLog->user->id]) : '' ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $contactsLog->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $contactsLog->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $contactsLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactsLog->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>

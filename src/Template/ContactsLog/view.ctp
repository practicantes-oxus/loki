<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ContactsLog $contactsLog
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Contacts Log'), ['action' => 'edit', $contactsLog->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Contacts Log'), ['action' => 'delete', $contactsLog->id], ['confirm' => __('Are you sure you want to delete # {0}?', $contactsLog->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Contacts Log'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contacts Log'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Contacts'), ['controller' => 'Contacts', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Contact'), ['controller' => 'Contacts', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Requeriments'), ['controller' => 'Requeriments', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Requeriment'), ['controller' => 'Requeriments', 'action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="contactsLog view large-9 medium-8 columns content">
    <h3><?= h($contactsLog->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Contact') ?></th>
            <td><?= $contactsLog->has('contact') ? $this->Html->link($contactsLog->contact->name, ['controller' => 'Contacts', 'action' => 'view', $contactsLog->contact->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Summary') ?></th>
            <td><?= h($contactsLog->summary) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Requeriment') ?></th>
            <td><?= $contactsLog->has('requeriment') ? $this->Html->link($contactsLog->requeriment->id, ['controller' => 'Requeriments', 'action' => 'view', $contactsLog->requeriment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('User') ?></th>
            <td><?= $contactsLog->has('user') ? $this->Html->link($contactsLog->user->name, ['controller' => 'Users', 'action' => 'view', $contactsLog->user->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($contactsLog->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Contacts Type') ?></th>
            <td><?= $this->Number->format($contactsLog->contacts_type) ?></td>
        </tr>
    </table>
</div>

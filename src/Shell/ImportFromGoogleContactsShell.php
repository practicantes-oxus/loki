<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\ORM\TableRegistry;
/*Configuracion Google Contacts Api*/
    use RapidWeb\GoogleOAuth2Handler\GoogleOAuth2Handler;
    use RapidWeb\GooglePeopleAPI\GooglePeople;
    use RapidWeb\GooglePeopleAPI\Contact;


        // const CLIENT_ID     = '816774634175-03br4978i0tjjeu5o5su7k42bcc7nlba.apps.googleusercontent.com';
        // const CLIENT_SECRET = 'tSalKV3bEpvR0dF0tcjI1jBK';
        // const REFRESH_TOKEN = '1/gCN2P8pfYPqpbPaXeklSMrj-A_xuEfHkOYCE7lwBYTy8UEfgR5mRQe1MBVAlv6R6';
        // const SCOPES       = ['https://www.googleapis.com/auth/userinfo.profile', 'https://www.googleapis.com/auth/contacts', 'https://www.googleapis.com/auth/contacts.readonly'];
/**
 * ImportFromGoogleContactsShell shell command.
 */
class ImportFromGoogleContactsShell extends Shell
{
    // public function main() {
    //     $this->out("Hola mundo");
    // }
    public function codes(){
        $codes = json_decode(file_get_contents('src/Shell/NormaliceClientData.json'),true);
        return $codes;
    }
    private function regulariceRut($string){
        /*Toma un string y lo convierte en un rut valido con formato: 12.345.678-9*/
        /*Obtengo la parte que esta antes del primer guion y guardo el caracter que viene despues del guion (digito verificador)*/
        $string = strtolower($string);
        $pos = strrpos($string ,'-');
        $verify = substr($string,($pos+1),1);
        if($pos!==false){
            $string = substr($string,0,$pos);

        }
        /*elimino cualquier caracter que no sea un numero*/
        $value_array = str_split($string);
        foreach($value_array as $k=>$character){
            if(!is_numeric($character)){
                unset($value_array[$k]);
            }
        }
        $string = implode('',$value_array);

        /*Agrego puntos cada 3 caracteres desde el final de la cadena hasta el principio*/
        $a = str_split(strrev($string),3);
        foreach($a as $k=>$b){
            $a[$k] = strrev($b);
        }
        $a = implode('.',array_reverse($a));

        /*Agrego digito verificador*/
        $a .= '-'.$verify;
        if(!is_numeric($verify) && $verify!='k'){
            return false;
        }
        return $a;
    }
    private function regulariceMonth($string){
        /*Obtengo una cadena de texto y devuelve la misma cadena en minusculas si es que pertenece a un mes o si es que tiene 'diciembre' dentro de ella. Ésto ultimo es porque en en google contacts hay 3 registros con 2 meses y todos poseen 'diciembre'. De lo contrario devuelve false*/
        $string = strtolower($string);
        if(strpos($string,'enero')!== false){return $string;}
        if(strpos($string,'febrero')!== false){return $string;}
        if(strpos($string,'marzo')!== false){return $string;}
        if(strpos($string,'abril')!== false){return $string;}
        if(strpos($string,'mayo')!== false){return $string;}
        if(strpos($string,'junio')!== false){return $string;}
        if(strpos($string,'julio')!== false){return $string;}
        if(strpos($string,'agosto')!== false){return $string;}
        if(strpos($string,'septiembre')!== false){return $string;}
        if(strpos($string,'octubre')!== false){return $string;}
        if(strpos($string,'noviembre')!== false){return $string;}
        if(strpos($string,'diciembre')!== false){return $string;}
        return false;
    }
    public function regulariceAccount($string){
        /*Obtiene un string y devule el mismo si es que contiene la palabra 'cuenta', 'banco', 'cta','cte' o 'bco'. De lo contrario devuelve false*/
        if(strpos(strtolower($string),'cuenta') !==false || strpos(strtolower($string),'banco') !==false || strpos(strtolower($string),'bco') !==false || strpos(strtolower($string),'cte') !==false || strpos(strtolower($string),'cta') !==false)
        {
            return ($string);
        }else{
            return (false);
        }
    }
    public function regularicePhoneNumber($string){
        /*Obtiene una cadena de texto y separa solo los numeros , de estos verifica si existe alguno con mas de 11 y digito y si empieza con '5699' o '5622' se asume que hubo un error al ingresarse y se elimina el numero repetido, si contiene 9 cifras se agrega '56', en todos los casos anteriormente mencionados se agrega un '+' al principio del string. Si no cumple ninguno de los requisitos anteriores se debuelve el false*/
        $value_array = str_split($string);
        foreach($value_array as $k=>$character){
            if(!is_numeric($character)){
                unset($value_array[$k]);
            }
        }
        $string = implode('',$value_array);
        if(strlen($string)>11){
            if(strpos($string,'5699')!==false && strpos($string,'5699')==0){
                $sub = substr($string,4);
                return ('+569'.$sub);
                //return ('error: '.$string.' xx:'.$sub);
            }
            else{
                if(strpos($string,'5622')!==false && strpos($string,'5622')==0){
                    $sub = substr($string,4);
                    return ('+562'.$sub);
                    //return ('error: '.$string.' xx:'.$sub);
                } else{
                    return false;
                    //return ('error1: '.$string);
                }
                
            }
        }else{
            if(strlen($string)==11){
                return '+'.$string;
            }else{
                if(strlen($string)==9){
                    return ('+56'.$string);                
                }else{
                    return false;
                }
            }
        }
        return $string;
    }
    public function regulariceLine($string){
        return preg_replace("/[\r\n|\n|\r]+/", " ", $string);
        if(strpos($string,'
            ') !==false){
            return $string;
        }
        return false;
    }
    private function stringToIsapreId($string){
        $string = strtolower($string);
        $oxus_code = $this->codes()['oxus_code'];
        if(array_key_exists($string,$oxus_code)){
            return ($oxus_code[$string]);
        }else{
            return NULL;
        }

    }
    private function stringToCommuneId($string){
        $string = mb_strtolower($string);
        $commune_code = $this->codes()['commune_code'];
        if(array_key_exists($string,$commune_code)){
            return ($commune_code[$string]);
        }else{
            return NULL;
        }
    }
    public function stringToAccountData($string){
        $string = str_replace(' ','',strtolower($string));
        $account_data_code = $this->codes()['account_data_code'];
        if(array_key_exists($string,$account_data_code)){
            $number = preg_replace('/[^0-9]+/', '', $string);
            $account_data_code[$string]['account_number'] = $number;
            return ($account_data_code[$string]);
        }else{
            //pr('asd');
            return null;
        }
    }
    public function main(){
        $google = TableRegistry::get('google');
        $Clients = TableRegistry::get('contacts');
         $juconoman = new GoogleOAuth2Handler($google);
         $people = new GooglePeople($juconoman);
         
        //$contact = $people->get('people/c1536847774564420126');
        $success = 0;
        $error = 0;
        foreach($people->all() as $contact) {
             //pr($contact);die;
            $client = $Clients->find('all')->where(['resource_name'=>$contact->resourceName])->first();
            if($client){
                continue;
            } 
            
            //$this->_io->overwrite('No importados: '.$success, 0, 20);
            
            $client = $Clients->newEntity();
            $client->resource_name = $contact->resourceName;
            //pr($contact->resourceName);
            if(!empty($contact->names)){
                foreach($contact->names as $name){
                    if(!empty($name->givenName)){
                        $client->names = utf8_decode(utf8_encode(($name->givenName)));
                    }
                    if(!empty($name->familyName)){
                        $client->last_name = utf8_decode(utf8_encode(($name->familyName)));
                    }

                }
            }
            if ($Clients->save($client)){
                $success ++;
            } else {
                $error ++;
            }
        }
        pr('Importacion finalizada: '.$success);
        die;
        pr('errores: '.$error);
        die;
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * TypeEnterpries Controller
 *
 * @property \App\Model\Table\TypeEnterpriesTable $TypeEnterpries
 *
 * @method \App\Model\Entity\TypeEnterpry[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TypeEnterpriesController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $typeEnterpries = $this->paginate($this->TypeEnterpries);

        $this->set(compact('typeEnterpries'));
    }

    /**
     * View method
     *
     * @param string|null $id Type Enterpry id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $typeEnterpry = $this->TypeEnterpries->get($id, [
            'contain' => []
        ]);

        $this->set('typeEnterpry', $typeEnterpry);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $typeEnterpry = $this->TypeEnterpries->newEntity();
        if ($this->request->is('post')) {
            $typeEnterpry = $this->TypeEnterpries->patchEntity($typeEnterpry, $this->request->getData());
            if ($this->TypeEnterpries->save($typeEnterpry)) {
                $this->Flash->success(__('The type enterpry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The type enterpry could not be saved. Please, try again.'));
        }
        $this->set(compact('typeEnterpry'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Type Enterpry id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $typeEnterpry = $this->TypeEnterpries->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $typeEnterpry = $this->TypeEnterpries->patchEntity($typeEnterpry, $this->request->getData());
            if ($this->TypeEnterpries->save($typeEnterpry)) {
                $this->Flash->success(__('The type enterpry has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The type enterpry could not be saved. Please, try again.'));
        }
        $this->set(compact('typeEnterpry'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Type Enterpry id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $typeEnterpry = $this->TypeEnterpries->get($id);
        if ($this->TypeEnterpries->delete($typeEnterpry)) {
            $this->Flash->success(__('The type enterpry has been deleted.'));
        } else {
            $this->Flash->error(__('The type enterpry could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

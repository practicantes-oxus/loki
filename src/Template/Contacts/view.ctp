<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>

<div class="row">
    <div class="col-md-4">
        <div class="box box-primary">
            <div class="box-body box-profile">              
                <h3 class="profile-username text-center"><?= h($contact->name) ?></h3>
                  <!-- <p class="text-muted text-center">Software Engineer</p> -->
                <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= __('Rut') ?></b> <a class="pull-right"><?= h($contact->rut) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Nombre') ?></b> <a class="pull-right"><?= h($contact->name) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Apellido') ?></b> <a class="pull-right"><?= h($contact->last_name) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Telefono') ?></b> <a class="pull-right"><?= h($contact->phone) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Empresa asociada') ?></b> <a class="pull-right"><?= h($contact->client->name)?></a>
                    </li>
                </ul>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
    <div class="col-md-8">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Requerimientos</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Nombre de requerimiento') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Tipo de requerimiento') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Cotización') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                        <? foreach($contact->requeriments as $requeriment): ?>
                            <tr>
                                <td><?= $this->Number->format($requeriment->id) ?></td>
                                <td><?= $requeriment->name_requeriment ?></td>
                                <td><?= h($requeriment->requirement_type_id) ?></td>
                                <td><?= h($requeriment->prices) ?></td>
                            </tr>
                        <? endforeach; ?>
                    </tbody>
                </table>
                <div class="paginator">
                    <ul class="pagination">
                        <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                        <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                        <?= $this->Paginator->numbers() ?>
                        <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                        <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
                    </ul>        
                </div>
            </div>
        </div>
    </div>
</div>
</div>
            
</div>
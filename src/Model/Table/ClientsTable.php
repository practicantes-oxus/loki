<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Clients Model
 *
 * @property \App\Model\Table\BanksTable|\Cake\ORM\Association\BelongsTo $Banks
 * @property \App\Model\Table\RegionsTable|\Cake\ORM\Association\BelongsTo $Regions
 *
 * @method \App\Model\Entity\Client get($primaryKey, $options = [])
 * @method \App\Model\Entity\Client newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Client[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Client|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Client patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Client[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Client findOrCreate($search, callable $callback = null, $options = [])
 */
class ClientsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('clients');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Banks', [
            'foreignKey' => 'bank_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Regions', [
            'foreignKey' => 'regions_id',
            'joinType' => 'INNER'
        ]);

        $this->hasMany('Contacts',[
            'foreignKey' => 'clients_id', 
        ]);
        $this->belongsTo('Instances', [
            'foreignKey' => 'instances_id',
            'joinType' => 'INNER'
        ]);
        

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->add('email', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Clients.email =' => $args['email']
                    ];
                    
                    return $query->where($conditions);
                }
            ])
            ->add('name', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Clients.name' => $args['name']
                    ];
                    return $query->where($conditions);
                }
            ])
            ->add('rut', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Clients.rut' => $args['rut']
                    ];
                    return $query->where($conditions);
                }
            ])
            ;
    
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('rut')
        //     ->maxLength('rut', 69)
        //     ->requirePresence('rut', 'create')
        //     ->notEmpty('rut');

        $validator
            ->scalar('name')
            ->maxLength('name', 69)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        // $validator
        //     ->scalar('address')
        //     ->maxLength('address', 69)
        //     ->requirePresence('address', 'create')
        //     ->notEmpty('address');

        // $validator
        //     ->email('email')
        //     ->requirePresence('email', 'create')
        //     ->notEmpty('email');

        // $validator
        //     ->scalar('phone')
        //     ->maxLength('phone', 69)
        //     ->requirePresence('phone', 'create')
        //     ->notEmpty('phone');

        // $validator
        //     ->dateTime('date')
        //     ->requirePresence('date', 'create')
        //     ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['bank_id'], 'Banks'));
        $rules->add($rules->existsIn(['regions_id'], 'Regions'));

        return $rules;
    }
}

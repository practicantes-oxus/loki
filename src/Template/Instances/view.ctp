<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-body box-profile">
           
            <h3 class="profile-username text-center"><?= h($instance->name) ?></h3>

            <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= __('Nombre') ?></b> <a class="pull-right"><?= h($instance->name) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Rut de la empresa') ?></b> <a class="pull-right"><?= h($instance->rut_enterprises) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Email') ?></b> <a class="pull-right"><?= h($instance->email) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Telefono') ?></b> <a class="pull-right"><?= h($instance->phone) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Dirección') ?></b> <a class="pull-right"><?= h($instance->address) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Contactos') ?></b> <a class="pull-right"><?= h($instance->contacts) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Tipo de instancia') ?></b> <a class="pull-right"><?= $instance->type_instance->name?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('fecha') ?></b> <a class="pull-right"><?= h($instance->date) ?></a>
                    </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="box-header with-border">
            
        </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Role') ?></th>
                    </tr>
                </thead>
                <tbody>
                        <? foreach($instance->users as $user): ?>
                    <tr>
                        <td><?= $this->Number->format($user->id) ?></td>
                            <td><?= $user->first_name ?></td>
                            <td><?= h($user->email) ?></td>
                            <td><?= h($user->roles_id) ?></td>
                    </tr>
                        <? endforeach; ?>
                </tbody>
            </table>

<div class="paginator">
    <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('Primero')) ?>
            <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
            <?= $this->Paginator->last(__('Ultimo') . ' >>') ?>
    </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>
<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Client Entity
 *
 * @property int $id
 * @property string $rut
 * @property string $name
 * @property string $address
 * @property string $email
 * @property string $phone
 * @property int $bank_id
 * @property int $regions_id
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \App\Model\Entity\Bank $bank
 * @property \App\Model\Entity\Region $region
 * @property \App\Model\Entity\Contact[] $contacts
 */
class Client extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true
    ];
}

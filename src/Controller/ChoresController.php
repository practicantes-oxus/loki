<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Chores Controller
 *
 * @property \App\Model\Table\ChoresTable $Chores
 *
 * @method \App\Model\Entity\Chore[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ChoresController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $users = $this->Chores->Users->find('list', [
            "keyField" => "id",
            "valueField" => "first_name"
        ]);
        $workOrders = $this->Chores->WorkOrders->find('list', ['limit' => 200]);
        $choresStatus = $this->Chores->Choresstatus->find('list', ['limit' => 200]);


        //codigo para poder filtrar datos de una instancia segun la cuenta logueada
        $instances = $this->Auth->user()['instances_id'];
        $data_search = $this->request->query;
        $query = $this->Chores->find('search', [
            'search' => $data_search])
        ->contain(['Instances', 'WorkOrders', 'ChoresStatus', 'Users']);

        /*con estas lineas de codigo nuevas se quitar el filtro por instancias al super admin
              pero primero hay que definir la funcion getCurrentdUser en el app controller
            */
            if ($this->getCurrentUser()['roles_id'] !== 4) {
                $query->where(['Chores.instances_id'=>$instances]);
            }
        
        $this->paginate = [
            'limit' => 5
        ];
        $chores = $this->paginate($query);

        $titleForLayout= 'Tareas';
        $breadCrumb= 'Inicio';
        $this->set(compact('chores', 'data_search', 'users', 'choresStatus', 'workOrders','titleForLayout', 'breadCrumb'));
    }

    /**
     * View method
     *
     * @param string|null $id Chore id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $chore = $this->Chores->get($id, [
            'contain' => ['Users', 'WorkOrders', 'ChoresStatus']
        ]);
        $titleForLayout= 'Tareas';
        $breadCrumb= 'Vista';
        $this->set('chore','titleForLayout', $chore, 'breadCrumb');
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $chore = $this->Chores->newEntity();
        if ($this->request->is('post')) {
            $chore = $this->Chores->patchEntity($chore, $this->request->getData());
            if ($this->Chores->save($chore))
             {
                $this->Flash->success(__('La tarea se a guardado.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La tarea no se pudo guardar. Intentelo nuevamente.'));
        }
        $users = $this->Chores->Users->find('list', [
            "keyField" => "id",
            "valueField" => "last_name"
        ]);
        
        $requeriment = $this->Chores->Requeriments->find('list', [
            "keyField" => "id",
            "valueField" => "name_requeriment"
        ]);
        $workOrders = $this->Chores->WorkOrders->find('list', []);
        $choresStatus = $this->Chores->Choresstatus->find('list', []);
        $instances = $this->Chores->Instances->find('list', []);
        $titleForLayout= 'Tareas';
        $breadCrumb= 'Agregar';
        $this->set(compact('nameRequeriments','chore', 'users', 'workOrders','breadCrumb', 'choresStatus', 'instances', 'titleForLayout', 'requeriment'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Chore id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $chore = $this->Chores->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $chore = $this->Chores->patchEntity($chore, $this->request->getData());
            if ($this->Chores->save($chore)) {
                $this->Flash->success(__('La tarea se a editado correctamente.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('La tarea no pudo editarce. Intentelo nuevamente.'));
        }
        $users = $this->Chores->Users->find('list', ['limit' => 200]);
        $workOrders = $this->Chores->WorkOrders->find('list', ['limit' => 200]);
        $choresStatus = $this->Chores->Choresstatus->find('list', ['limit' => 200]);
        $titleForLayout= 'Tareas';
        $breadCrumb= 'Editar';
        $this->set(compact('chore', 'users', 'workOrders', 'choresStatus', 'titleForLayout', 'breadCrumb'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Chore id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $chore = $this->Chores->get($id);
        if ($this->Chores->delete($chore)) {
            $this->Flash->success(__('La tarea a sido eliminada correctamente.'));
        } else {
            $this->Flash->error(__('La tarea no pudo ser eliminada. Intentelo nuevamente.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

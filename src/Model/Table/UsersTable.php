<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Users Model
 *
 * @property \App\Model\Table\InstancesTable|\Cake\ORM\Association\BelongsTo $Instances
 * @property \App\Model\Table\RolesTable|\Cake\ORM\Association\BelongsTo $Roles
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 */
class UsersTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->belongsTo('Instances', [
            'foreignKey' => 'instances_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Roles', [
            'foreignKey' => 'roles_id',
            'joinType' => 'INNER'
        ]);
        //codigo para mostrar datos de otra tabla 
        $this->hasMany('chores',[
            'foreignKey' => 'users_id'
        ]);

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->add('instances_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Users.instances_id =' => $args['instances_id']
                    ];
                    
                    return $query->where($conditions);
                }
            ])
            ->add('first_name', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Users.first_name' => $args['first_name']
                    ];
                    return $query->where($conditions);
                }
            ])
            ->add('rut', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Users.rut' => $args['rut']
                    ];
                    return $query->where($conditions);
                }
            ])
            ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->scalar('rut')
        //     ->maxLength('rut', 25)
        //     ->requirePresence('rut', 'create')
        //     ->notEmpty('rut');

        $validator
            ->scalar('first_name')
            ->maxLength('first_name', 30)
            ->requirePresence('first_name', 'create')
            ->notEmpty('first_name');

        // $validator
        //     ->scalar('last_name')
        //     ->maxLength('last_name', 30)
        //     ->requirePresence('last_name', 'create')
        //     ->notEmpty('last_name');

        // $validator
        //     ->scalar('phone')
        //     ->maxLength('phone', 30)
        //     ->requirePresence('phone', 'create')
        //     ->notEmpty('phone');

        // $validator
        //     ->email('email')
        //     ->requirePresence('email', 'create')
        //     ->notEmpty('email');

        $validator
            ->scalar('password')
            ->maxLength('password', 60)
            ->requirePresence('password', 'create')
            ->notEmpty('password');

        // $validator
        //     ->dateTime('date')
        //     ->requirePresence('date', 'create')
        //     ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['email']));
        $rules->add($rules->existsIn(['instances_id'], 'Instances'));
        $rules->add($rules->existsIn(['roles_id'], 'Roles'));

        return $rules;
    }
}
/*
 * @mixin \Search\Model\Behavior\SearchBehavior
 */
class PostsTable extends Table
{
    /**
     * @param array $config
     *
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        
    }
}
<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;



/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link https://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
    */
    
    /*aqui se define la funcion getCurrenUser para que al super admin pueda ver
    ver todos los datos de las empresas
    */
    protected function getCurrentUser(){
        return $this->Auth->user();
    }

    public function initialize() 
    {
        parent::initialize();


        $this->loadComponent('Search.Prg', [
        // This is default config. You can modify "actions" as needed to make
        // the PRG component work only for specified methods.
        'actions' => ['index', 'lookup']
        ]);

        /*$this->loadComponent('RequestHandler', [
            'enableBeforeRedirect' => false,
        ]);*/
        $this->loadComponent('Flash');
        //codigo antiguo
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => [
                        'username' => 'email',
                        'password' => 'password'
                    ],
                ]
           ],
           'loginAction' => [
               'controller' => 'Users',
               'action' => 'login'
           ],
           'authError' => 'ingrese sus datos',
           'loginRedirect' => [
               'controller' => 'Users',
               'action' => 'redirectUrl'
           ],
           'logoutRedirect' => [
               'controller' => 'Users',
               'action' => 'login'
           ],
           'unauthorizedRedirect' => $this->referer()

        ]);



        // Codigo nuevo de ACL Manager   
        /*$this->loadComponent('Auth', [
            'authorize' => [
                'Acl.Actions' => ['actionPath' => 'controllers/']
            ],
            'loginAction' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'login'
            ],
            'loginRedirect' => [
                'plugin' => false,
                'controller' => 'Posts',
                'action' => 'index'
            ],
            'logoutRedirect' => [
                'plugin' => false,
                'controller' => 'Pages',
                'action' => 'display'
            ],
            'unauthorizedRedirect' => [
                'plugin' => false,
                'controller' => 'Users',
                'action' => 'login',
                'prefix' => false
            ],
            'authError' => 'You are not authorized to access that location.',
            'flash' => [
                'element' => 'error'
            ]
        ]);
        $this->Auth->allow();*/

        /*
         * Enable the following component for recommended CakePHP security settings.
         * see https://book.cakephp.org/3.0/en/controllers/components/security.html
         */
        //$this->loadComponent('Security');
    }
    public function beforeRender(Event $event)
    {
        

        $adminPublicActions = array('login','resetPassword', 'newPassword','resetPassword'); 
        if (in_array($this->request->params['action'], $adminPublicActions)){
            $this->viewBuilder()->layout('login');
        }
        // aqui tambien se definio el getCurrentUser
        $current_user = $this->getCurrentUser();
        //pr($current_user); die;
        /*if(empty($current_user))
        { 
            $this->redirect(['controller' => 'Users' ,'action' => 'login']);
        }*/
       
        $this->set('current_user',$current_user);
    }
    

    public function isAuthorized($user)
    {
        
        
        $administrador =[
            'Clients'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Contacts'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Requeriments'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'WorkOrders'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Chores'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Roles'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Users'=> [
                'index', 'add',  'edit', 'login', 'logout', 'view', 'home', 'delete'
            ],
            'Instances'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ]
        ];

        $ejecutivo =[
            'Requeriments'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view'
            ],
            'WorkOrders'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view'
            ],
            'Chores'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view'
            ],
            'Users'=> [
                'home'
            ]
        ];

        $trabajador =[
            'Requeriments'=> [
                'index', 'add', 'edit', 'view'
            ],
            'WorkOrders'=> [
                'index', 'add', 'edit', 'view'
            ],
            'Chores'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view'
            ],
            'Users'=> [
                 'index', 'login', 'logout', 'view', 'home', 'redirectUrl'
            ],
            'Instances'=> [
                'index', 'login', 'logout', 'view'
            ],
            
        ];
        //super administrador
        $superAdministrador =[
            'Clients'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Contacts'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Requeriments'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'WorkOrders'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Chores'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Roles'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ],
            'Users'=> [
                'index', 'add',  'edit', 'login', 'logout', 'view', 'delete', 'home'
            ],
            'Instances'=> [
                'index', 'add', 'edit', 'login', 'logout', 'view', 'delete'
            ]
            
        ];

        $params = $this->request->params;
        if($user)
        //pr($user['roles_id']); die;
        {
            switch($user['roles_id']){
                case 1: 
                    if(!empty($administrador[$params['controller']])){
                        if(in_array($this->request->action, $administrador[$params['controller']]))
                        {
                            return true;
                        }
                    }
                case 2: 
                    if(!empty($ejecutivo[$params['controller']])){
                        if(in_array($this->request->action, $ejecutivo[$params['controller']]))
                        {
                            return true;
                        }
                    }
                case 3:
                    if(!empty($trabajador[$params['controller']])){
                        if(in_array($this->request->action, $trabajador[$params['controller']]))
                        {
                            return true;
                        }
                    }
                case 4:
                    if(!empty($superAdministrador[$params['controller']])){
                        if(in_array($this->request->action, $superAdministrador[$params['controller']]))
                        {
                            return true;
                        }
                    }    
            }
        }
        return false;


    }
    
    
}
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<!--div class="users view large-9 medium-8 columns content">
    <h3><?= h($user->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Rut') ?></th>
            <td><?= h($user->rut) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('First Name') ?></th>
            <td><?= h($user->first_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Last Name') ?></th>
            <td><?= h($user->last_name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Phone') ?></th>
            <td><?= h($user->phone) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($user->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Password') ?></th>
            <td><?= h($user->password) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($user->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Instances Id') ?></th>
            <td><?= $user->instance->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Roles Id') ?></th>
            <td><?= $user->role->name ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($user->date) ?></td>
        </tr>
    </table>
</div-->

<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-body box-profile">
           
            <h3 class="profile-username text-center"><?= h($user->first_name) ?></h3>

            <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= __('Nombre') ?></b> <a class="pull-right"><?= h($user->first_name) ?> <?= h($user->last_name) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Rut') ?></b> <a class="pull-right"><?= h($user->rut) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Email') ?></b> <a class="pull-right"><?= h($user->email) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Telefono') ?></b> <a class="pull-right"><?= h($user->phone) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Intancia') ?></b> <a class="pull-right"><?= $user->instance->name ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Rol') ?></b> <a class="pull-right"><?= $user->role->name ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Fecha') ?></b> <a class="pull-right"><?= h($user->date) ?></a>
                    </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="box-header with-border">
            
        </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Rol') ?></th>
                    </tr>
                </thead>
                <tbody>
                        <? foreach($user->chores as $chore): ?>
                    <tr>
                        <td><?= $this->Number->format($chore->id) ?></td>
                            <td><?= $chore->name ?></td>
                            <td><?= h($chore->date) ?></td>
                            <td><?= h($chore->description) ?></td>
                    </tr>
                        <? endforeach; ?>
                </tbody>
            </table>

<div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
        <?= $this->Paginator->first(' ' . __('Primera')) ?>
        <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('Siguente') . ' ') ?>
        <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
    </ul>
</div>
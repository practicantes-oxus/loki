<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * TypeEnterpries Model
 *
 * @method \App\Model\Entity\TypeEnterpry get($primaryKey, $options = [])
 * @method \App\Model\Entity\TypeEnterpry newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\TypeEnterpry[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\TypeEnterpry|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeEnterpry|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\TypeEnterpry patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\TypeEnterpry[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\TypeEnterpry findOrCreate($search, callable $callback = null, $options = [])
 */
class TypeEnterpriesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('type_enterpries');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        $validator
            ->scalar('name')
            ->maxLength('name', 60)
            ->requirePresence('name', 'create')
            ->notEmpty('name');

        return $validator;
    }
}

<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * ContactsLog Controller
 *
 * @property \App\Model\Table\ContactsLogTable $ContactsLog
 *
 * @method \App\Model\Entity\ContactsLog[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ContactsLogController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Contacts', 'Requeriments', 'Users']
        ];
        $contactsLog = $this->paginate($this->ContactsLog);

        $this->set(compact('contactsLog'));
    }

    /**
     * View method
     *
     * @param string|null $id Contacts Log id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $contactsLog = $this->ContactsLog->get($id, [
            'contain' => ['Contacts', 'Requeriments', 'Users']
        ]);

        $this->set('contactsLog', $contactsLog);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $contactsLog = $this->ContactsLog->newEntity();
        if ($this->request->is('post')) {
            $contactsLog = $this->ContactsLog->patchEntity($contactsLog, $this->request->getData());
            if ($this->ContactsLog->save($contactsLog)) {
                $this->Flash->success(__('The contacts log has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contacts log could not be saved. Please, try again.'));
        }
        $contacts = $this->ContactsLog->Contacts->find('list', ['limit' => 200]);
        $requeriments = $this->ContactsLog->Requeriments->find('list', ['limit' => 200]);
        $users = $this->ContactsLog->Users->find('list', ['limit' => 200]);
        $this->set(compact('contactsLog', 'contacts', 'requeriments', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Contacts Log id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $contactsLog = $this->ContactsLog->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $contactsLog = $this->ContactsLog->patchEntity($contactsLog, $this->request->getData());
            if ($this->ContactsLog->save($contactsLog)) {
                $this->Flash->success(__('The contacts log has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The contacts log could not be saved. Please, try again.'));
        }
        $contacts = $this->ContactsLog->Contacts->find('list', ['limit' => 200]);
        $requeriments = $this->ContactsLog->Requeriments->find('list', ['limit' => 200]);
        $users = $this->ContactsLog->Users->find('list', ['limit' => 200]);
        $this->set(compact('contactsLog', 'contacts', 'requeriments', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Contacts Log id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $contactsLog = $this->ContactsLog->get($id);
        if ($this->ContactsLog->delete($contactsLog)) {
            $this->Flash->success(__('The contacts log has been deleted.'));
        } else {
            $this->Flash->error(__('The contacts log could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}

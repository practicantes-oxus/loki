<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Requeriments Model
 *
 * @property \App\Model\Table\ContactsTable|\Cake\ORM\Association\BelongsTo $Contacts
 * @property \App\Model\Table\RequerimentsTypesTable|\Cake\ORM\Association\BelongsTo $RequerimentsTypes
 *
 * @method \App\Model\Entity\Requeriment get($primaryKey, $options = [])
 * @method \App\Model\Entity\Requeriment newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Requeriment[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Requeriment|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Requeriment|bool saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Requeriment patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Requeriment[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Requeriment findOrCreate($search, callable $callback = null, $options = [])
 */
class RequerimentsTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('requeriments');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->hasMany('Chores', [
            'foreignKey' => 'id_requeriment'
        ]);

        $this->belongsTo('Contacts', [
            'foreignKey' => 'contacts_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('RequerimentsTypes', [
            'foreignKey' => 'requirement_type_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('priorityOfRequirement', [
            'foreignKey' => 'priority_id',
            'joinType' => 'INNER'
        ]);
        $this->belongsTo('Instances', [
            'foreignKey' => 'instances_id',
            'joinType' => 'INNER'
        ]);
       //codigo para mostrar datos de otra tabla 
        $this->hasMany('workOrders',[
            'foreignKey' => 'requeriments_id', 
        ]);

        // Add the behaviour to your table
        $this->addBehavior('Search.Search');

        // Setup search filter using search manager
        $this->searchManager()
            ->add('name_requeriment', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Requeriments.name_requeriment =' => $args['name_requeriment']
                    ];
                    
                    return $query->where($conditions);
                }
            ])
            ->add('contacts_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Requeriments.contacts_id' => $args['contacts_id']
                    ];
                    return $query->where($conditions);
                }
            ])
            ->add('priority_id', 'Search.Callback', [
                'callback' => function($query, $args, $filter) {
                    $conditions = [
                        'Requeriments.priority_id' => $args['priority_id']
                    ];
                    return $query->where($conditions);
                }
            ])
            ;
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');

        // $validator
        //     ->integer('priority')
        //     ->requirePresence('priority', 'create')
        //     ->notEmpty('priority');

        // $validator
        //     ->scalar('name_requeriment')
        //     ->requirePresence('name_requeriment', 'create')
        //     ->notEmpty('name_requeriment');
          

        $validator
            ->scalar('contacts_id')
            ->requirePresence('contacts_id', 'create')
            ->notEmpty('contacts_id');
            
        $validator
            ->scalar('description')
            ->requirePresence('description', 'create')
            ->notEmpty('description');

        // $validator
        //     ->dateTime('date')
        //     ->requirePresence('date', 'create')
        //     ->notEmpty('date');

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['contacts_id'], 'Contacts'));
        $rules->add($rules->existsIn(['requirement_type_id'], 'RequerimentsTypes'));

        return $rules;
    }
}

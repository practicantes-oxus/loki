<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Requeriment Entity
 *
 * @property int $id
 * @property int $contacts_id
 * @property int $requirement_type_id
 * @property int $priority
 * @property string $prices
 * @property \Cake\I18n\FrozenTime $date
 *
 * @property \App\Model\Entity\Contact $contact
 * @property \App\Model\Entity\RequerimentsType $requeriments_type
 */
class Requeriment extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        '*' => true,
        'id' => false
    ];
}

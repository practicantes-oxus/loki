    <div class="row">
        <div class="col-md-3 col-md-push-9">

            <div class="box box-info"><!-- Acciones -->
                <div class="box-header with-border">
                    <h3 class="box-title">Acciones</h3>
                </div>
                <?= $this->Form->create($client) ?>
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="btn-toolbar">
                            <?= $this->Html->link('<i class="fa fa-repeat fa-side"></i> Volver a la Lista',['controller' => 'clients', 'action' => 'index'],['class' => 'btn btn-info option', 'escape' => false])?>
                    <?= $this->Form->button(__('<i class="fa fa-save fa-side"></i>   Guardar'), ['class' => 'btn btn-info option'],['escape' => false]); ?>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-9 col-md-pull-3">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Agregar Cliente</h3>
                </div>
            
                <div class="box-body">
                    <div class="row"><!--fila 1 -->
                        <div class="col-md-4">
                            <? echo $this->Form->control('rut', ['class' => 'form-control', 'label' => 'Rut']);?>
                        </div>
                        <div class="col-md-4">
                            <? echo $this->Form->control('name', ['class' => 'form-control', 'label' => 'Nombre']);?>
                        </div>
                        <div class="col-md-4">
                             <? echo $this->Form->control('address', ['class' => 'form-control', 'label' => 'Dirección']);?>
                        </div>
                    </div>
                    <br>
                    <div class="row"> <!--fila 2 -->
                        <div class="col-md-4">
                            <? echo $this->Form->control('phone', ['class' => 'form-control', 'label' => 'Telefono']);?>
                        </div>
                        <div class="col-md-4">
                            <? echo $this->Form->control('email', ['class' => 'form-control', 'label' => 'Email']);?>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <? echo $this->Form->control('bank_id', ['label' => 'Bancos', 'options' => $banks,'class' => 'form-control', 'empty' => 'Seleccione']);?>
                            </div>
                        </div>
                    </div>    
                    <br>
                    <div class="row"><!-- fila 3 -->
                    
                        <div class="col-md-4 col-precio">
                            <? echo $this->Form->control('regions_id', ['label' => 'Regiones', 'options' => $regions,'class' => 'form-control', 'empty' => 'Seleccione']);?>
                        </div>
                        <div class="col-md-4 col-precio">
                            <? echo $this->Form->control('instances_id', [ 'options' => $instances,'class' => 'form-control', 'label' => 'Instancia']);?>
                        </div>
                    </div>
                </div>
                <?= $this->Form->end() ?>
            </div>
        </div> 
<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Client $client
 */
?>





<div class="row">
  <div class="col-md-3">
    <div class="box box-primary">
      <div class="box-body box-profile">        
        <h3 class="profile-username text-center"><?= $client->name?></h3>
          <ul class="list-group list-group-unbordered">
            <li class="list-group-item">
              <b><?= __('Rut') ?></b> <a class="pull-right"><?= h($client->rut) ?></a>
            </li>
            <li class="list-group-item">
              <b><?= __('Nombre') ?></b> <a class="pull-right"><?= h($client->name) ?></a>
            </li>
            <li class="list-group-item">
              <b><?= __('Dirección') ?></b> <a class="pull-right"><?= h($client->address) ?></a>
            </li>
            <li class="list-group-item">
              <b><?= __('Email') ?></b> <a class="pull-right"><?= h($client->email) ?></a>
            </li>
            <li class="list-group-item">
              <b><?= __('Telefono') ?></b> <a class="pull-right"><?= h($client->phone) ?></a>
            </li>
            <li class="list-group-item">
              <b><?= __('Banco') ?></b> <a class="pull-right"><?= h($client->bank->name) ?></a>
            </li>
            <li class="list-group-item">
              <b><?= __('Region') ?></b> <a class="pull-right"><?=  $client->region->name ?></a>
            </li>
          </ul>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
      <div class="col-md-9">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Contactos</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">
            <table class="table table-hover">
              <thead>
                <tr>
                  <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('Email') ?></th>
                  <th scope="col"><?= $this->Paginator->sort('Telefono') ?></th>
                </tr>
              </thead>
              <tbody>
                <? foreach($client->contacts as $contact): ?>
                  <tr>
                    <td><?= $this->Number->format($contact->id) ?></td>
                    <td><?= $contact->name ?></td>
                    <td><?= h($client->email) ?></td>
                    <td><?= h($contact->phone) ?></td>
                  </tr>
                <? endforeach; ?>
              </tbody>
            </table>
            <div class="paginator">
              <ul class="pagination">
                <?= $this->Paginator->first('<< ' . __('Primera')) ?>
                <?= $this->Paginator->prev('< ' . __('Anterior')) ?>
                <?= $this->Paginator->numbers() ?>
                <?= $this->Paginator->next(__('Siguiente') . ' >') ?>
                <?= $this->Paginator->last(__('Ultima') . ' >>') ?>
              </ul>
              <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
            </div>
          </div>
        </div>
      </div>
/*jQuery(document).ready(function($) {
    updateDocModal($('#cause_idDocSave').val());
        
    $('.datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('#buttonForm').on('click', function(event) {
        var validation = true;
        var name = $('#name').val();
        var document_name = $('#document').val();
        var commentary = $('#commentary').val();
        var date = $('#date').val();
        var document_type_id = $('#document_type_id').val();
        var cause_id = $('#cause_id').val();
        if(name.length == 0){
            validation = false;
            $('#divName').focus();
            $('#divName').addClass('has-error');
        }else{
            $('#divName').removeClass('has-error');
        }
        if(document_name.length == 0){
            validation = false;
            $('#divDoc').focus();
            $('#divDoc').addClass('has-error');
        }else{
            $('#divDoc').removeClass('has-error');
        }
        if(commentary.length == 0){
            validation = false;
            $('#divCommentary').focus();
            $('#divCommentary').addClass('has-error');
        }else{
            $('#divCommentary').removeClass('has-error');
        }
        if(date.length == 0){
            validation = false;
            $('#divDate').focus();
            $('#divDate').addClass('has-error');
        }else{
            $('#divDate').removeClass('has-error');
        }
        if (document_type_id == 0){
            validation = false;
            $('#divDocType').focus();
            $('#divDocType').addClass('has-error');
        }else{
            $('#divDocType').removeClass('has-error');
        }
        $('#formDoc').submit(function(event) {
            if(validation){
                return true;
            }else{
                return false;
            }
        });
    }); 
    
    $('#formDocGenerator').submit(function(){
        if($('#planillaIdInput').val()){
            return true;
        }else{
            swal("Porfavor", "Seleccione una <b>plantilla</b>", "error");
            return false;
        }
    });

});
function updateDocModal(id){
    $.ajax({
        url: '/CausesDocuments/doc-modal',
        data: {'cause_id':id},
        type: 'post',
        success: function(response){
            
            $('#documentSaves').html(response);
        },
    });
}*/
function updateDocModal(cause_id){
    
    $.ajax({
        url: '/CausesDocuments/doc-modal',
        data: {'cause_id':cause_id},
        type: 'post',
        success: function(response){
            $('#documentSaves').html(response);
            $('#documentSaves').css('display','block');
        },
    });
}
jQuery(document).ready(function($) {
    $('.datetimepicker1').datetimepicker({
        format: 'YYYY-MM-DD'
    });
    $('.pickermonth').datetimepicker({
        format: 'MMMM'
    });

    $('#planillaIdInput').val('');

    $('#planillaIdInput').change(function(){
        $.ajax({
            url: '/DocTemplates/has-optional-vars/'+ $(this).val(),
            beforeSend: function(){
            },
            success: function(hasOptionalVars){
                if(hasOptionalVars == 1){
                    $('#other-input').show();
                } else{
                    $('#other-input').hide();
                }
            },
        });
    });
    /*$('.span-button').click(function(){ //se hace referencia a la funcion click de la clase input-button
        var id  = $(this).attr('id'); //se setea la variable con el atributo id
        $('.input-button#'+ id).val($(this).html()); //se le fija el valor de id al in put por su  clase
        $('.input-button#'+ id).show(); //se muestra el input
        $(this).hide(); //se esconde el span que tiene la id correspondiente
    });*/


    $('.input-button').focus(function(){ //se hace referencia a la perdida de foco para la clase del input
        $('#appear-button').show();
        $(this).change(function(){
            window.onbeforeunload = function (e) {
                return false;
            }
        });
    });

    $('#appear-button').click(function(){
        $('#formDataEdit').submit(function(event) {
            event.preventDefault();
        });
        var id = $(this).attr('data-id-cause');
        var data = $('#formDataEdit').serialize();
        $.ajax({
            url: '/Causes/editInView/'+id,
            type: 'post',
            data: data,
            beforeSend: function(){
                swal({
                    type: "info",
                    title: "Espera!",
                    text: "Tus cambios se estan registrando",
                    button: false,
                    allowOutsideClick: false,
                    showConfirmButton: false,
                    allowEscapeKey: false,
                });
            },
            success: function(response){
                //swal("Respuesta", response, "info");
                if(response == 1){
                    swal("Muy Bien!", "Tus cambios se han registrado correctamente", "success");
                }else /*if(response == 0)*/{
                    swal("Error", 'Ha ocurrido un error en el proceso de registrar tus cambios <br><b><u>Verifica tus valores</b></u>', "error");
                }
                updateDocModal($('#causeId_DocTemplate').val());
            },
        });
    });
    /*$('.docLeft').change(function(event) {
        
        var id = $(this).attr('data-id');
        var document_left = $(this).val();
        swal({
            title: 'Estas seguro que deseas subir este archivo ?',
            text: document_left,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#6460ac',
            cancelButtonColor: '#fe0b0b',
            confirmButtonText: 'Si, subir este archivo',
            cancelButtonText: 'No, seleccionar otro'
        }).then((result) => {
            if (result.value) {
                document_left = $(this)[0].files[0];
                var formData = new FormData();
                formData.append("document",document_left);
                $.ajax({
                    url: '/CausesDocuments/edit/'+id,
                    type: 'post',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        swal({
                            type: "info",
                            title: "Espera!",
                            text: "Tu documento se esta subiendo",
                            button: false,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            allowEscapeKey: false,
                        });
                    },
                    success: function(response){
                        //swal("Respuesta", response, "info");
                        if(response == 1){
                            swal("Muy Bien!", "Tu archivo se ha subido de forma correcta", "success");
                        }else if(response == 0){
                            swal("Error", "Ha ocurrido un error en el proceso de subir el archivo <br><b><u> Intente mas tarde </b></u>", "error");
                        }
                        updateDocModal($('#appear-button').attr('data-id-cause'));
                    },
                });
            }else {
                updateDocModal($('#appear-button').attr('data-id-cause'));
            }
        });
    });*/

    $('#typeDocSave').on('change',function(){
        var val = $(this).val();
        var type = typeOfDocuments[val];
        if (type == 0) { //Si es de causa
            
            $('#labelDateReception').html('Fecha de recepción');
            $('#divDateEmissionDocSave').show();
            $('#divDocSave').removeClass('col-md-6');
            $('#divDocSave').addClass('col-md-12');
        } else if(type == 1){ //Si es de cliente
            
            $('#labelDateReception').html('Fecha de vencimiento');
            $('#divDateEmissionDocSave').hide();
            $('#divDocSave').removeClass('col-md-12');
            $('#divDocSave').addClass('col-md-6');
        }
        var documentTypeName = document_types[val];
        
        var validationForCommentary = 0;
        var validationForName = 0;
        $.each( document_types, function(i, val) { //Se recorren los tipos de documentos
            if ($.trim($('#nameDocSave').val()) == val) {
                validationForName = 1;
            }
            if($.trim($('#commentaryDocSave').val()) == val) {
               validationForCommentary = 1; 
            }
        });
        
        
        if($.trim($('#nameDocSave').val()).length < 1 || validationForName == 1){
            $('#nameDocSave').val(documentTypeName);
        }
        if($.trim($('#commentaryDocSave').val()).length < 1 || validationForCommentary){
            $('#commentaryDocSave').val(documentTypeName);
        }
    });

    $('#formAddDocument').on('click', function(){
        var validation = true;
        var name = $('#nameDocSave').val();
        var commentary = $('#commentaryDocSave').val();
        var date = $('#dateDocSave').val();
        var date_emission = $('#dateEmissionDocSave').val();
        var document_type_id = $('#typeDocSave').val();
        var cause_id = $('#cause_idDocSave').val();
        if($('#documentDocSave').val()){
            var document_name = $('#documentDocSave')[0].files[0];
        }else{
            var document_name = "";
        }
        if(name.length < 1){
            validation = false;
            $('#divNameDocSave').focus();
            $('#divNameDocSave').addClass('has-error');
        }else{
            $('#divNameDocSave').removeClass('has-error');
        }
        if (typeOfDocuments[document_type_id] != 1) { //SI NO ES DOCUMENTO DE CLIENTE, SE VALIDA FECHA
            if(date.length < 1){
                validation = false;
                $('#divDateReceptionDocSave').focus();
                $('#divDateReceptionDocSave').addClass('has-error');
            }else{
                $('#divDateReceptionDocSave').removeClass('has-error');
            }
            if(date_emission.length < 1){
                validation = false;
                $('#divDateEmissionDocSave').focus();
                $('#divDateEmissionDocSave').addClass('has-error');
            }else{
                $('#divDateEmissionDocSave').removeClass('has-error');
            }
        }
        if (document_type_id < 1){
            validation = false;
            $('#divTypeDocSave').focus();
            $('#divTypeDocSave').addClass('has-error');
        }else{
            $('#divTypeDocSave').removeClass('has-error');
        }
        if(validation){
            var formData = new FormData();
            var val = $('#typeDocSave').val();
            var type = typeOfDocuments[val];
            if (type == 0) {
                formData.append("name",name);
                formData.append("document",document_name);
                formData.append("commentary",commentary);
                formData.append("date",date);
                formData.append("date_emission",date_emission);
                formData.append("document_type_id",document_type_id);
                formData.append("cause_id",cause_id);
                $.ajax({
                    url: '/CausesDocuments/add',
                    data: formData,
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        swal({
                            type: "info",
                            title: "Espera!",
                            text: "Se esta guardando la informacion",
                            button: false,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            allowEscapeKey: false,
                        });
                    },
                    success: function(response){
                        //swal("<pr>", response, "success");
                        if(response == 1){
                            swal("Muy Bien!", "Se ha guardado la informacion de forma correcta", "success");
                        }else if(response == 0){
                            swal("Error", "Ha ocurrido un error en el proceso de guardar la informacion<br><b><u> Intente mas tarde </b></u>", "error");
                        }
                        updateDocModal($('#cause_idDocSave').val());
                    },
                });
            } else if(type == 1) {
                formData.append("name",name);
                formData.append("commentary",commentary);
                formData.append("cause_id",cause_id);
                formData.append("document_type_id",document_type_id);
                formData.append("document",document_name);
                formData.append("expire_date",date);
                formData.append('on',1); //VALIDA QUE VENGA DESDE LA VISTA PRECAUSE-VIEW, PARA ASI SACAR EL CLIENTE DE LA CAUSA ¡REVISAR CLIENTSDOC CONTROLLER!
                $.ajax({
                    url: '/ClientsDocuments/add',
                    data: formData,
                    type: 'post',
                    cache: false,
                    contentType: false,
                    processData: false,
                    beforeSend: function(){
                        swal({
                            type: "info",
                            title: "Espera!",
                            text: "Se esta guardando la informacion",
                            button: false,
                            allowOutsideClick: false,
                            showConfirmButton: false,
                            allowEscapeKey: false,
                        });
                    },
                    success: function(response){
                        swal("<pr>", response, "success");
                        if(response == 1){
                            swal("Muy Bien!", "Se ha guardado la informacion de forma correcta", "success");
                        }else if(response == 0){
                            swal("Error", "Ha ocurrido un error en el proceso de guardar la informacion<br><b><u> Intente mas tarde </b></u>", "error");
                        }
                        updateDocModal($('#cause_idDocSave').val());
                    },
                });
            }
        }else{
            swal("Porfavor", "Rellene todos los campos en rojo", "error");
        }
    });
});



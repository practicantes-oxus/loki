<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\WorkOrder $workOrder
 */
?>
<!--div class="workOrders view large-9 medium-8 columns content">
    <h3><?= h($workOrder->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($workOrder->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Requeriment') ?></th>
            <td><?= $workOrder->has('requeriment') ? $this->Html->link($workOrder->requeriment->id, ['controller' => 'Requeriments', 'action' => 'view', $workOrder->requeriment->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Work Order Status') ?></th>
            <td><?= $workOrder->has('work_order_status') ? $this->Html->link($workOrder->work_order_status->name, ['controller' => 'WorkOrderStatus', 'action' => 'view', $workOrder->work_order_status->id]) : '' ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($workOrder->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Date') ?></th>
            <td><?= h($workOrder->date) ?></td>
        </tr>
    </table>
</div-->



<div class="col-md-3">
    <div class="box box-primary">
        <div class="box-body box-profile">
           
            <h3 class="profile-username text-center"><?= h($workOrder->name) ?></h3>

            <ul class="list-group list-group-unbordered">
                    <li class="list-group-item">
                        <b><?= __('Nombre') ?></b> <a class="pull-right"><?= h($workOrder->name) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Requerimiento') ?></b> <a class="pull-right"><?= $workOrder->has('requeriment') ? $this->Html->link($workOrder->requeriment->id, ['controller' => 'Requeriments', 'action' => 'view', $workOrder->requeriment->id]) : '' ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Estado Orden de Trabajo') ?></b> <a class="pull-right"><?= $workOrder->has('work_order_status') ? $this->Html->link($workOrder->work_order_status->name, ['controller' => 'WorkOrderStatus', 'action' => 'view', $workOrder->work_order_status->id]) : '' ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Id') ?></b> <a class="pull-right"><?= $this->Number->format($workOrder->id) ?></a>
                    </li>
                    <li class="list-group-item">
                        <b><?= __('Fecha') ?></b> <a class="pull-right"><?= h($workOrder->date) ?></a>
                    </li>
            </ul>
        </div>
    </div>
</div>
<div class="col-md-9">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Tareas</h3>
        </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Nombre') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Fecha') ?></th>
                            <th scope="col"><?= $this->Paginator->sort('Descripción') ?></th>
                    </tr>
                </thead>
                <tbody>
                        <? foreach($workOrder->chores as $chore): ?>
                    <tr>
                        <td><?= $this->Number->format($chore->id) ?></td>
                            <td><?= $chore->name ?></td>
                            <td><?= h($chore->date) ?></td>
                            <td><?= h($chore->description) ?></td>
                    </tr>
                        <? endforeach; ?>
                </tbody>
            </table>

<div class="box-footer clearfix">
    <ul class="pagination pagination-sm no-margin pull-right">
        <?= $this->Paginator->first(' ' . __('Primera')) ?>
        <?= $this->Paginator->prev(' ' . __('Anteriror')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('Siguente') . ' ') ?>
        <?= $this->Paginator->last(__('Ultima ') . ' ') ?>
    </ul>
</div>